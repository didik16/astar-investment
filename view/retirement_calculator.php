<?php include 'config/includeWithVariables.php'; ?>


<!doctype html>
<html class="no-js" lang="en">

<?php includeWithVariables('view/include/script_header.php', array('title' => 'Retirement Planner')); ?>

<body>
    <?php include 'include/header.php'; ?>


    <!-- breadcrumbs Area Start-->
    <div class="breadcrumbs-area bg-overlay-dark bg-9 id=" id="paralax" style="background-image:linear-gradient(rgb(255 255 255 / 47%) 68%, rgb(31 32 32) 100%), url(<?php echo ASSET_URL ?>img/banner/22.jpg)"">
        <div class=" container">
        <div class="row">
            <div class="col-md-12">
                <div class="breadcrumbs-text text-left">
                    <h2>Retirement Planner</h2>
                    <!-- <div class="breadcrumbs-bar">
                            <ul class="breadcrumbs">
                                <li>GET TO KNOW YOUR BROKERAGE</li>
                            </ul>
                        </div> -->
                </div>
            </div>
        </div>
    </div>
    </div>
    <!-- breadcrumbs Area End -->

    <!-- <section class="section section-md bg-default novi-background"> -->
    <div class="container p-5">
        <div class="row justify-content-md-center ">
            <section class="section section-md bg-default novi-background">
                <div class="container">
                    <div class="row">

                        <iframe src="<?php echo BASE_URL ?>calculator" height="950" width="90%" title="Iframe Example" style="border:none;"></iframe>

                        <p style="color:black">**Please note, all calculations made with the Astar Investment Limited retirement planner are for illustrative purposes only. For a more information and an in depth look at how a Astar Investment Limited advisor can assist you to prepare for your retirement, please contact one of our representatives today. </p>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <!-- </section> -->


    <?php include 'include/footer.php'; ?>

</body>

</html>