<?php include 'config/includeWithVariables.php'; ?>


<!doctype html>
<html class="no-js" lang="en">

<?php includeWithVariables('view/include/script_header.php', array('title' => 'Fixed Income Trading')); ?>

<body>
    <?php include 'include/header.php'; ?>


    <!-- breadcrumbs Area Start-->
    <div class="breadcrumbs-area bg-overlay-dark bg-9" id="paralax" style="background-image:linear-gradient(rgb(255 255 255 / 47%) 68%, rgb(31 32 32) 100%), url(<?php echo ASSET_URL ?>img/banner/15.jpg)">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumbs-text text-left">
                        <h2>FIXED INCOME TRADING</h2>
                        <h2></h2>
                        <div class="breadcrumbs-bar">
                            <ul class="breadcrumbs">
                                <!-- <li><a href="index.html">HOME</a></li> -->
                                <!-- <li>Astar Investment Limited ARE PROUD TO CATER TO A DIVERSE CLIENT BASE, AND THEIR RESEARCH TEAM ALLOWS THEM TO FOCUS ON GIVING EACH CLIENT THE INFORMATION AND OPPORTUNITIES TO MATCH THEIR NEEDS.</li> -->
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- breadcrumbs Area End -->

    <!-- <section class="section section-md bg-default novi-background"> -->
    <div class="container p-5">
        <div class="row justify-content-md-center ">

            <div class="col-md-8">
                <!-- Blurb circle-->
                <article class="blurb blurb-circle">
                    <div class="unit flex-sm-row unit-spacing-md">

                        <div class="unit__body">
                            <h4 class="mb-3">Fixed Income trading</h4>

                            <p>Astar Investment Limited’s fixed income trading team is made up of a diverse group of financial professionals who between them share over a century of expertise in a multitude of fixed income sectors. Our team of specialists have a direct line of communication to the trading team, and as a client of Astar Investment Limited Group you will gain in depth access to the team and utilize their wealth of experience as they work to create a unique and diverse portfolio with you. One of the main focuses of a portfolio built surrounding a fixed income strategy is for those investors who are much more risk adverse, Fixed income strategies are a moderately safe way to enhance an existing portfolio, or create a new portfolio for the inexperienced investor, as it provides a steady residual income over time, with low to moderate exposure in the markets.</p>

                            <p>Your Astar Investment Limited portfolio manager will work closely with you to design, create and manage a portfolio that will assist you with reaching your goals, while adhering to your tolerance to risk. By utilizing the vast knowledge of our team of advisors, and combining that knowledge with information backed by our research team, we are confident in our ability to push your portfolio above and beyond your expectations.</p>
                        </div>
                    </div>
                </article>
            </div>


            <?php include 'include/market-update.php'; ?>

        </div>
    </div>
    <!-- </section> -->


    <?php include 'include/footer.php'; ?>

</body>

</html>