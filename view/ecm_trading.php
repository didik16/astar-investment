<?php include 'config/includeWithVariables.php'; ?>


<!doctype html>
<html class="no-js" lang="en">

<?php includeWithVariables('view/include/script_header.php', array('title' => 'ECM Trading')); ?>

<body>
    <?php include 'include/header.php'; ?>


    <!-- breadcrumbs Area Start-->
    <div class="breadcrumbs-area bg-overlay-dark bg-9" id="paralax" style="background-image:linear-gradient(rgb(255 255 255 / 47%) 68%, rgb(31 32 32) 100%), url(<?php echo ASSET_URL ?>img/banner/21.jpg)">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumbs-text text-left">
                        <h2>ECM TRADING</h2>
                        <!-- <div class="breadcrumbs-bar">
                            <ul class="breadcrumbs">
                                <li>GET TO KNOW YOUR BROKERAGE</li>
                            </ul>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- breadcrumbs Area End -->

    <!-- <section class="section section-md bg-default novi-background"> -->
    <div class="container p-5">
        <div class="row justify-content-md-center ">

            <div class="col-md-8">
                <!-- Blurb circle-->
                <article class="blurb blurb-circle">
                    <div class="unit flex-sm-row unit-spacing-md">

                        <div class="unit__body">
                            <p>Astar Investment Limited has built their long standing reputation based on success and team work. Our reach within the equity capital market sector is unrivaled in the finance industry. The co-operation between Astar Investment Limited and their partners has been of vital importance when it comes to our clients success. Working this way has enabled our teams to have better access to more profitable investment opportunities. Our team handle a very large part of both the distribution and research of opportunities that our research and analysts bring to them.</p>

                            <p>Astar Investment Limited’s equity capital market team provides their clients with greater access to IPO’s, Secondary offerings and a wide array of other alternative investment strategies which would compliment your existing portfolio, and increase the profitability of your investments. When it comes to the markets there are many different potential investment opportunities that you will be able to choose from, working closely with you we are able to offer you both relevant and lucrative investment choices that suit your goals.</p>
                        </div>
                    </div>
                </article>
            </div>


            <?php include 'include/market-update.php'; ?>

        </div>
    </div>
    <!-- </section> -->


    <?php include 'include/footer.php'; ?>

</body>

</html>