<?php include 'config/includeWithVariables.php'; ?>


<!doctype html>
<html class="no-js" lang="en">

<?php includeWithVariables('view/include/script_header.php', array('title' => 'Research')); ?>

<body>
    <?php include 'include/header.php'; ?>


    <!-- breadcrumbs Area Start-->
    <div class="breadcrumbs-area bg-overlay-dark bg-9" id="paralax" style="background-image:linear-gradient(rgb(255 255 255 / 47%) 68%, rgb(31 32 32) 100%), url(<?php echo ASSET_URL ?>img/banner/10.jpg)">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumbs-text text-left">
                        <h2>RESEARCH</h2>
                        <h2></h2>
                        <div class="breadcrumbs-bar">
                            <ul class="breadcrumbs">
                                <!-- <li><a href="index.html">HOME</a></li> -->
                                <li>FORD BECKETT ARE PROUD TO CATER TO A DIVERSE CLIENT BASE, AND THEIR RESEARCH TEAM ALLOWS THEM TO FOCUS ON GIVING EACH CLIENT THE INFORMATION AND OPPORTUNITIES TO MATCH THEIR NEEDS.</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- breadcrumbs Area End -->

    <!-- <section class="section section-md bg-default novi-background"> -->
    <div class="container p-5">
        <div class="row justify-content-md-center ">

            <div class="col-md-8">
                <!-- Blurb circle-->
                <article class="blurb blurb-circle">
                    <div class="unit flex-sm-row unit-spacing-md">

                        <div class="unit__body">
                            <p>In today’s constantly evolving financial industry, you need to choose a brokerage who can maintain the pace. Ford Beckett’s research team is the epicenter of our great results for our clients. Not only are the markets constantly changing, our clients goals and plans change when unforeseen circumstances arise in their lives, our portfolio managers work hand in hand with our researchers to ensure that our clients portfolio changes with their needs. Our researchers go far deeper into a company than a mere google search, and we search all angles from the investment opportunities inception up until the current day, which enables us to see past trends that may be missed by an average investor. We also work closely with our vast network of external and internal assistants to determine companies that we deem undervalued, so that we can gain as much insight as necessary to ensure we are offering our clients the best possible financial solutions.</p>

                            <p>Ford Beckett are proud to cater to a diverse client base, and understand that depending on their geographical location, and economical background, their tolerance to risk, and exposure in the markets varies as much as they do. This means that our research team must always be on top of a large number of investment opportunities, with a variety of risk levels, this also allows us to diversify portfolios and include investment vehicles that our clients may have not heard about just yet.</p>

                            <p>We know that our researchers are of vital importance when it comes to your success, so we make sure that we are in constant expansion to bring our clients the best information that they deserve. Our team is comprised of ex portfolio managers, financial market experts, global economists, and industry specific experts, having such a diverse team working together gives us a real edge over the competition.</p>

                            <p>The Ford Beckett research team is made up of a large number of financial professionals including, financial advisors, economists, market analysts, and market experts with decades of experience in the financial industry. Every member of our research department are dedicated to providing our brokers and portfolio managers in depth accurate information regarding companies and investment opportunities, in a timely manner so that we are able to provide you, our clients, with the best information to push your portfolios to new highs.</p>


                        </div>
                    </div>
                </article>
            </div>


            <?php include 'include/market-update.php'; ?>

        </div>
    </div>
    <!-- </section> -->


    <?php include 'include/footer.php'; ?>

</body>

</html>