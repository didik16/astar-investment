<?php include 'config/includeWithVariables.php'; ?>


<!doctype html>
<html class="no-js" lang="en">

<?php includeWithVariables('view/include/script_header.php', array('title' => 'Option Trading')); ?>

<body>
    <?php include 'include/header.php'; ?>


    <!-- breadcrumbs Area Start-->
    <div class="breadcrumbs-area bg-overlay-dark bg-9" id="paralax" style="background-image:linear-gradient(rgb(255 255 255 / 47%) 68%, rgb(31 32 32) 100%), url(<?php echo ASSET_URL ?>img/banner/12.jpg)">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumbs-text text-left">
                        <h2>OPTIONS TRADING</h2>
                        <h2></h2>
                        <div class="breadcrumbs-bar">
                            <ul class="breadcrumbs">
                                <!-- <li>Astar Investment Limited BELIEVE THAT WITH THE RIGHT PEOPLE WE CAN CHANGE THE WORLD OF INVESTMENTS</li> -->
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- breadcrumbs Area End -->

    <!-- <section class="section section-md bg-default novi-background"> -->
    <div class="container p-5">
        <div class="row justify-content-md-center ">

            <div class="col-md-8">
                <!-- Blurb circle-->
                <article class="blurb blurb-circle">
                    <div class="unit flex-sm-row unit-spacing-md">

                        <div class="unit__body">
                            <p>Options contracts give the holder of said contracts the rights to buy or sell the underlying security at a selected strike up price until the expiration date of the aforementioned contract. Options are classed as derivatives, which allow the holder to benefit from the upside of the chosen stocks price movement, while capping out on the losses to only the cost of the option itself. They provide leverage of stock ownership at a fragment of the cost with a much more controlled downside dollar value risk. For every options contract you own, you control 100 shares of the chosen stock, which would mean that a holder of 10 options call contacts controls an equivalent of 1000 shared of the stock in which they have chosen an option of.</p>

                            <p>Options contracts themselves, are traded on a variety of options exchanges, as the price of options move with the underlying security price movements. Unlike stocks, options can lose a large portion of their value by the expiration date of the contract, where the underlying stock is trading. With options, directional bets are often made with them, however the contracts more often than not end up expiring due to a low volatility of the underlying security.</p>

                        </div>
                    </div>
                </article>
            </div>


            <?php include 'include/market-update.php'; ?>

        </div>
    </div>
    <!-- </section> -->


    <?php include 'include/footer.php'; ?>

</body>

</html>