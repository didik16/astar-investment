<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Home Four || Financo</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Favicon Icon -->
    <link rel="shortcut icon" type="image/x-icon" href="../asset/img/favicon.ico">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,600" rel="stylesheet">

    <!-- All css here -->
    <link rel="stylesheet" href="../asset/css/bootstrap.min.css">
    <link rel="stylesheet" href="../asset/css/font-awesome.min.css">
    <link rel="stylesheet" href="../asset/css/shortcode/shortcodes.css">
    <link rel="stylesheet" href="../asset/css/animate.css">
    <link rel="stylesheet" href="../asset/css/owl.carousel.css">
    <link rel="stylesheet" href="../asset/css/style.css">
    <link rel="stylesheet" href="../asset/css/responsive.css">

</head>

<body>
    <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

    <!-- Header Area Start -->
    <header class="header-four-area">
        <div class="header-top blue-bg fix d-lg-block d-md-block d-none">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-3">
                        <span>Welcome to Financo</span>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="header-icons">
                            <a href="#"><i class="fa fa-facebook"></i></a>
                            <a href="#"><i class="fa fa-twitter"></i></a>
                            <a href="#"><i class="fa fa-google-plus"></i></a>
                            <a href="#"><i class="fa fa-linkedin"></i></a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-3">
                        <div class="search-container">
                            <form action="#" method="post">
                                <input placeholder="" type="text">
                                <button><i class="fa fa-search"></i></button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-main bg-white">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="header-main-content">
                            <div class="header-info">
                                <img src="../asset/img/icons/phone-2.png" alt="">
                                <div class="header-info-text">
                                    <h4>+88 (012) 564 979 56</h4>
                                    <span>We are open 9 am - 10pm</span>
                                </div>
                            </div>
                        </div>
                        <div class="logo-wrapper">
                            <div class="logo">
                                <a href="index.html"><img src="../asset/../asset/img/logo/logo.png" alt="FINANCO" /></a>
                            </div>
                        </div>
                        <div class="quote-btn">
                            <button>Get a Quote</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Main Menu Area Start -->
        <div class="mainmenu-area fixed header-sticky d-lg-block d-md-block d-none">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-12">
                        <div class="main-menu text-center">
                            <nav>
                                <ul>
                                    <li><a href="index.html">HOME</a>
                                        <ul class="submenu">
                                            <li><a href="index.html">Home Style 1</a></li>
                                            <li><a href="index-2.html">Home Style 2</a></li>
                                            <li><a href="index-3.html">Home Style 3</a></li>
                                            <li><a href="index-4.html">Home Style 4</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="about.html">ABOUT US</a>
                                        <ul class="submenu">
                                            <li><a href="about-2.html">About Us 2</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="#">CASES</a>
                                        <ul class="submenu megamenu">
                                            <li><a href="#">Megamenu List</a>
                                                <ul>
                                                    <li><a href="about.html">ABOUT US</a></li>
                                                    <li><a href="about-2.html">About Us 2</a></li>
                                                    <li><a href="services.html">Service</a></li>
                                                    <li><a href="services-2.html">Service 2</a></li>
                                                    <li><a href="single-service.html">Single Service</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="#">Megamenu List</a>
                                                <ul>
                                                    <li><a href="project.html">Project</a></li>
                                                    <li><a href="project-2.html">Project 2</a></li>
                                                    <li><a href="project-3.html">Project 3</a></li>
                                                    <li><a href="project-4.html">Project 4</a></li>
                                                    <li><a href="project-details.html">Project Details</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="#">Megamenu List</a>
                                                <ul>
                                                    <li><a href="blog.html">BLOG</a></li>
                                                    <li><a href="blog-left-sidebar.html">Blog Left Sidebar</a></li>
                                                    <li><a href="blog-right-sidebar.html">Blog Right Sidebar</a></li>
                                                    <li><a href="blog-details.html">Blog Details</a></li>
                                                    <li><a href="contact.html">Contact</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                    <li><a href="services.html">SERVICES</a>
                                        <ul class="submenu">
                                            <li><a href="services-2.html">Services 2</a></li>
                                            <li><a href="single-service.html">Single Service</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="project.html">PROJECTS</a>
                                        <ul class="submenu">
                                            <li><a href="project-2.html">Projects 2</a></li>
                                            <li><a href="project-3.html">Projects 3</a></li>
                                            <li><a href="project-4.html">Projects 4</a></li>
                                            <li><a href="project-details.html">Project Details</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="blog.html">BLOG</a>
                                        <ul class="submenu">
                                            <li><a href="blog-left-sidebar.html">Blog Left Sidebar</a></li>
                                            <li><a href="blog-right-sidebar.html">Blog Right Sidebar</a></li>
                                            <li><a href="blog-details.html">Blog Details</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="#">SHOP</a></li>
                                    <li><a href="contact.html">CONTACT</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Main Menu Area Start -->
        <!-- Mobile Menu Area start -->
        <div class="mobile-menu-area d-lg-none d-md-none d-block">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12">
                        <div class="mobile-menu">
                            <nav id="dropdown">
                                <ul>
                                    <li><a href="index.html">HOME</a>
                                        <ul class="submenu">
                                            <li><a href="index-2.html">Home Style 2</a></li>
                                            <li><a href="index-3.html">Home Style 3</a></li>
                                            <li><a href="index-4.html">Home Style 4</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="about.html">ABOUT US</a>
                                        <ul class="submenu">
                                            <li><a href="about-2.html">About Us 2</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="services.html">SERVICES</a>
                                        <ul class="submenu">
                                            <li><a href="services-2.html">Services 2</a></li>
                                            <li><a href="single-service.html">Single Service</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="project.html">PROJECTS</a>
                                        <ul class="submenu">
                                            <li><a href="project-2.html">Projects 2</a></li>
                                            <li><a href="project-3.html">Projects 3</a></li>
                                            <li><a href="project-4.html">Projects 4</a></li>
                                            <li><a href="project-details.html">Project Details</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="blog.html">BLOG</a>
                                        <ul class="submenu">
                                            <li><a href="blog-left-sidebar.html">Blog Left Sidebar</a></li>
                                            <li><a href="blog-right-sidebar.html">Blog Right Sidebar</a></li>
                                            <li><a href="blog-details.html">Blog Details</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="contact.html">CONTACT</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Mobile Menu Area end -->
    </header>
    <!-- Header Area End -->
    <!-- Background Area Start -->
    <div class="slider-four-area">
        <div class="slider-wrapper">
            <div class="single-slide" style="background-image: url('../asset/../asset/img/slider/4.jpg');">
                <div class="banner-content">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-10 offset-lg-1">
                                <div class="text-content-wrapper">
                                    <div class="text-content text-center">
                                        <h3 class="title1 pt-65">WE PROVIDE BEST FINANCIAL SOLUTIONS</h3>
                                        <h1 class="title2">FOR YOUR FINANCIAL PLAN</h1>
                                        <p>We have over 25 year’s of experience in Finance and Business management so we can make your business more successful you can trust us</p>
                                        <div class="banner-readmore">
                                            <a class="button banner-btn" href="#">VIEW SERVICES</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="single-slide" style="background-image: url('../asset/../asset/img/slider/4.jpg');">
                <div class="banner-content">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-10 offset-lg-1">
                                <div class="text-content-wrapper slide-two">
                                    <div class="text-content text-center">
                                        <h3 class="title1 pt-65">WE PROVIDE BEST FINANCIAL SOLUTIONS</h3>
                                        <h1 class="title2">FOR YOUR FINANCIAL PLAN</h1>
                                        <p>We have over 25 year’s of experience in Finance and Business management so we can make your business more successful you can trust us</p>
                                        <div class="banner-readmore">
                                            <a class="button banner-btn" href="#">VIEW SERVICES</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Background Area End -->
    <!-- Services Area Start -->
    <div class="service-area pt-120 pb-105">
        <div class="container">
            <div class="row">
                <div class="col-xl-8 offset-xl-2 col-lg-10 offset-lg-1">
                    <div class="section-title text-center mb-55">
                        <h2>Our Services</h2>
                        <p>Financo amet sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-6">
                    <div class="single-item">
                        <span class="service-image">
                            <img src="../asset/img/icons/plan.png" alt="">
                        </span>
                        <div class="service-text">
                            <h4>Financial Planning</h4>
                            <p>Financo consectetur adipiscing elit, sed do eiusmod tempor incididnt </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="single-item">
                        <span class="service-image">
                            <img src="../asset/img/icons/cash.png" alt="">
                        </span>
                        <div class="service-text">
                            <h4>Cash Investment</h4>
                            <p>Financo consectetur adipiscing elit, sed do eiusmod tempor incididnt </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="single-item">
                        <span class="service-image">
                            <img src="../asset/img/icons/brif.png" alt="">
                        </span>
                        <div class="service-text">
                            <h4>Financial Consultancy</h4>
                            <p>Financo consectetur adipiscing elit, sed do eiusmod tempor incididnt </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="single-item">
                        <span class="service-image">
                            <img src="../asset/img/icons/hand.png" alt="">
                        </span>
                        <div class="service-text">
                            <h4>Personal Insurance</h4>
                            <p>Financo consectetur adipiscing elit, sed do eiusmod tempor incididnt </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="single-item">
                        <span class="service-image">
                            <img src="../asset/img/icons/commo.png" alt="">
                        </span>
                        <div class="service-text">
                            <h4>Commodities Planning</h4>
                            <p>Financo consectetur adipiscing elit, sed do eiusmod tempor incididnt </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="single-item">
                        <span class="service-image">
                            <img src="../asset/img/icons/reti.png" alt="">
                        </span>
                        <div class="service-text">
                            <h4>Retirement Planning</h4>
                            <p>Financo consectetur adipiscing elit, sed do eiusmod tempor incididnt </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Services Area End -->
    <!-- Information Area Start -->
    <div class="information-four-area bg-light fix">
        <div class="information-chart">
            <div id="chartContainer2" style="height: 345px; width: 100%;"></div>
        </div>
        <div class="information-text">
            <h3><span>xxxxWe Provide Best</span></h3>
            <h2>Financial Plan for You</h2>
            <p>https://www.fordbeckett.com/images/why_index.jpg</p>
            <p>Financo amet, ut perspiciatis unde omnis iste natus error sit voluptatem accusantium que laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis ipsum dolor sit amet, consectetur </p>
            <a href="#" class="default-button">Learn morexxx</a>
        </div>
    </div>
    <!-- Information Area End -->
    <!-- About Area Start -->
    <div class="about-four-area bg-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-7">
                    <div class="about-text">
                        <h3>We are <span>Financo</span></h3>
                        <h2>Your Complete Financial Solutions </h2>
                        <h4>Time is money, So lets start with proper financial plan</h4>
                        <p>Financo amet, consectetur adipiscing elit, sed do eiusmod tempornt ut labore et dolore magna aliqua. Ut enim ad minim iam, quis norud exercitation ullamco</p>
                        <p>Financo amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua enim ad minimiam</p>
                    </div>
                    <div class="about-text-container">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-12">
                                <div class="single-content">
                                    <h4 class="single-title">Financial Service</h4>
                                    <p>Financo consectetur adipiscing elit, sed do eiusmod tempor incididnt</p>
                                </div>
                                <div class="single-content">
                                    <h4 class="single-title">Business Analysis</h4>
                                    <p>Financo consectetur adipiscing elit, sed do eiusmod tempor incididnt</p>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-12">
                                <div class="single-content">
                                    <h4 class="single-title">Best Consutancy</h4>
                                    <p>Financo consectetur adipiscing elit, sed do eiusmod tempor incididnt</p>
                                </div>
                                <div class="single-content">
                                    <h4 class="single-title">24/7 Support</h4>
                                    <p>Financo consectetur adipiscing elit, sed do eiusmod tempor incididnt</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- About Area End -->
    <!-- Features Area Start -->
    <div class="features-area bg-light fix">
        <div class="feature-left-column">
            <img src="../asset/img/banner/2.jpg" alt="">
        </div>
        <div class="feature-right-column">
            <div class="coustom-col-10">
                <div class="feature-text">
                    <h3><span>Why You</span></h3>
                    <h2>Choose Financo</h2>
                    <p>Financo amet, ut perspiciatis unde omnis iste natus error sit voluptatem accusantium ipsum dolor sit amet, etur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna </p>
                </div>
            </div>
            <div class="custom-row">
                <div class="coustom-col-5">
                    <div class="single-item">
                        <span class="service-image">
                            <img src="../asset/img/icons/rocket.png" alt="">
                        </span>
                        <div class="service-text">
                            <h4>Fast Loan Approval</h4>
                            <p>Financo consectetur adipiscing elit, sed do eiusmod tempor incididnt </p>
                        </div>
                    </div>
                </div>
                <div class="coustom-col-5">
                    <div class="single-item">
                        <span class="service-image">
                            <img src="../asset/img/icons/team.png" alt="">
                        </span>
                        <div class="service-text">
                            <h4>Dedicated Team</h4>
                            <p>Financo consectetur adipiscing elit, sed do eiusmod tempor incididnt </p>
                        </div>
                    </div>
                </div>
                <div class="coustom-col-5">
                    <div class="single-item">
                        <span class="service-image">
                            <img src="../asset/img/icons/doc.png" alt="">
                        </span>
                        <div class="service-text">
                            <h4>Free Documentation</h4>
                            <p>Financo consectetur adipiscing elit, sed do eiusmod tempor incididnt </p>
                        </div>
                    </div>
                </div>
                <div class="coustom-col-5">
                    <div class="single-item">
                        <span class="service-image">
                            <img src="../asset/img/icons/refi.png" alt="">
                        </span>
                        <div class="service-text">
                            <h4>Refinancing</h4>
                            <p>Financo consectetur adipiscing elit, sed do eiusmod tempor incididnt </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Features Area End -->
    <!-- Advertise Area Start -->
    <div class="advertise-area bg-2 overlay-green">
        <div class="container">
            <div class="row">
                <div class="col-xl-8 offset-xl-2 col-lg-10 offset-lg-1">
                    <div class="advertise-text text-white text-center">
                        <h2>We Provide Best Financial Theme for your Business</h2>
                        <p>We have over 25 year’s of experience in Finance and Business management so we can make your business more successful you can trust us and we care about it</p>
                        <a href="#" class="default-button btn-white">Buy now</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Advertise Area End -->
    <!-- Projects Area Start -->
    <div class="projects-area ptb-120">
        <div class="container">
            <div class="row">
                <div class="col-xl-8 offset-xl-2 col-lg-10 offset-lg-1">
                    <div class="section-title text-center mb-55">
                        <h2>Our Projects</h2>
                        <p>Financo amet sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row custom-row grid fix">
                <div class="col-lg-4 col-md-6 col-12 grid-item">
                    <div class="project-style">
                        <div class="project-image">
                            <img src="../asset/img/project/13.jpg" alt="" />
                        </div>
                        <div class="project-hover">
                            <a class="project-icon image-popup-no-margins" href="../asset/img/project/13.jpg"><img src="../asset/img/icons/src.png" alt=""></a>
                            <div class="project-text">
                                <h4><a href="project.html">Financial Planning</a></h4>
                                <p>Financo consectetur adipiscing elit, sed do eiusmod tempor incididnt </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-12 grid-item">
                    <div class="project-style">
                        <div class="project-image">
                            <img src="../asset/img/project/14.jpg" alt="" />
                        </div>
                        <div class="project-hover">
                            <a class="project-icon image-popup-no-margins" href="../asset/img/project/14.jpg"><img src="../asset/img/icons/src.png" alt=""></a>
                            <div class="project-text">
                                <h4><a href="project.html">Financial Planning</a></h4>
                                <p>Financo consectetur adipiscing elit, sed do eiusmod tempor incididnt </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-12 grid-item">
                    <div class="project-style">
                        <div class="project-image">
                            <img src="../asset/img/project/15.jpg" alt="" />
                        </div>
                        <div class="project-hover">
                            <a class="project-icon image-popup-no-margins" href="../asset/img/project/15.jpg"><img src="../asset/img/icons/src.png" alt=""></a>
                            <div class="project-text">
                                <h4><a href="project.html">Financial Planning</a></h4>
                                <p>Financo consectetur adipiscing elit, sed do eiusmod tempor incididnt </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-12 grid-item">
                    <div class="project-style">
                        <div class="project-image">
                            <img src="../asset/img/project/16.jpg" alt="" />
                        </div>
                        <div class="project-hover">
                            <a class="project-icon image-popup-no-margins" href="../asset/img/project/16.jpg"><img src="../asset/img/icons/src.png" alt=""></a>
                            <div class="project-text">
                                <h4><a href="project.html">Financial Planning</a></h4>
                                <p>Financo consectetur adipiscing elit, sed do eiusmod tempor incididnt </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-12 grid-item">
                    <div class="project-style">
                        <div class="project-image">
                            <img src="../asset/img/project/17.jpg" alt="" />
                        </div>
                        <div class="project-hover">
                            <a class="project-icon image-popup-no-margins" href="../asset/img/project/17.jpg"><img src="../asset/img/icons/src.png" alt=""></a>
                            <div class="project-text">
                                <h4><a href="project.html">Financial Planning</a></h4>
                                <p>Financo consectetur adipiscing elit, sed do eiusmod tempor incididnt </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-12 grid-item">
                    <div class="project-style">
                        <div class="project-image">
                            <img src="../asset/img/project/18.jpg" alt="" />
                        </div>
                        <div class="project-hover">
                            <a class="project-icon image-popup-no-margins" href="../asset/img/project/18.jpg"><img src="../asset/img/icons/src.png" alt=""></a>
                            <div class="project-text">
                                <h4><a href="project.html">Financial Planning</a></h4>
                                <p>Financo consectetur adipiscing elit, sed do eiusmod tempor incididnt </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Projects Area End -->
    <!-- Asked Area Start -->
    <div class="asked-area pt-120 pb-85 bg-light">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="sub_title">
                        <h3>Reqeust a Call Back</h3>
                        <p>Financo amet sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua enim ad minim veniam</p>

                    </div>
                    <form id="contact-form" action="call-back-mail.php" method="post">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-box">
                                    <input type="text" name="name" placeholder="Name" required>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-box">
                                    <input type="email" name="email" placeholder="Email" required>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-box">
                                    <input type="text" name="subject" placeholder="Subject">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-box">
                                    <input type="text" name="phone" placeholder="Phone">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-box">
                                    <textarea name="message" placeholder="Message"></textarea>
                                    <button type="submit" class="default-button submit-btn">Submit Now</button>
                                </div>
                                <p class="form-messege"></p>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-lg-6">
                    <div class="default-panel">
                        <div class="sub_title">
                            <h3>Frequently Ask Qustion</h3>
                            <p>Financo amet sit amet, consectetur adipiscing elit, sed do eiusmod temp incididunt ut labore et dolore magna aliqua enim ad minim veniam</p>
                        </div>
                        <div class="accordion" id="accordionExample">
                            <div class="card">
                                <div class="card-header" id="headingOne">
                                    <h4 class="card-title">
                                        <a role="button" data-toggle="collapse" aria-expanded="true" href="#one" aria-controls="one">
                                            <span></span> What kind of Financial Consultancy you need
                                        </a>
                                    </h4>
                                </div>
                                <div id="one" class="collapse show" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordionExample">
                                    <div class="card-body">
                                        <p>Sed ut perspiciatis unde omnis iste natus error sit tatem accusantium doloremque laudantium tam rem riam, eaque ipsa quae ab illo invet </p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" role="tab" id="headingTwo">
                                    <h4 class="card-title">
                                        <a class="collapsed" data-toggle="collapse" aria-expanded="false" href="#two" aria-controls="two">
                                            <span></span> How we help you for your Business
                                        </a>
                                    </h4>
                                </div>
                                <div id="two" class="collapse" role="tabpanel" aria-labelledby="headingTwo" data-parent="#accordionExample">
                                    <div class="card-body">
                                        <p>Sed ut perspiciatis unde omnis iste natus error sit tatem accusantium doloremque laudantium tam rem riam, eaque ipsa quae ab illo invet </p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" role="tab" id="headingThree">
                                    <h4 class="card-title">
                                        <a class="collapsed" data-toggle="collapse" aria-expanded="false" href="#three" aria-controls="three">
                                            <span></span> Financial Advice
                                        </a>
                                    </h4>
                                </div>
                                <div id="three" class="collapse" role="tabpanel" aria-labelledby="headingThree" data-parent="#accordionExample">
                                    <div class="card-body">
                                        <p>Sed ut perspiciatis unde omnis iste natus error sit tatem accusantium doloremque laudantium tam rem riam, eaque ipsa quae ab illo invet </p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" role="tab" id="headingFour">
                                    <h4 class="card-title">
                                        <a class="collapsed" data-toggle="collapse" aria-expanded="false" href="#four" aria-controls="four">
                                            <span></span> What is the best Business plan for you
                                        </a>
                                    </h4>
                                </div>
                                <div id="four" class="collapse" role="tabpanel" aria-labelledby="headingFour" data-parent="#accordionExample">
                                    <div class="card-body">
                                        <p>Sed ut perspiciatis unde omnis iste natus error sit tatem accusantium doloremque laudantium tam rem riam, eaque ipsa quae ab illo invet </p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" role="tab" id="headingFive">
                                    <h4 class="card-title">
                                        <a class="collapsed" data-toggle="collapse" aria-expanded="false" href="#five" aria-controls="five">
                                            <span></span> Which is the right plan for Retirement
                                        </a>
                                    </h4>
                                </div>
                                <div id="five" class="collapse" role="tabpanel" aria-labelledby="headingFive" data-parent="#accordionExample">
                                    <div class="card-body">
                                        <p>Sed ut perspiciatis unde omnis iste natus error sit tatem accusantium doloremque laudantium tam rem riam, eaque ipsa quae ab illo invet </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Asked Area End -->
    <!-- Fun Factor Start -->
    <div class="fun-factor-area bg-3 overlay-green pt-100 fix pb-95">
        <div class="container">
            <div class="fun-custom-row">
                <div class="fun-custom-column">
                    <div class="single-fun-factor">
                        <div class="text-icon block">
                            <img src="../asset/img/icons/cup.png" alt="" class="mr-15">
                            <h2><span class="counter">750</span></h2>
                        </div>
                        <h4>Cup Of Coffee</h4>
                    </div>
                </div>
                <div class="fun-custom-column">
                    <div class="single-fun-factor">
                        <div class="text-icon block">
                            <img src="../asset/img/icons/check.png" alt="" class="mr-15">
                            <h2><span class="counter">350</span></h2>
                        </div>
                        <h4>Case Complete</h4>
                    </div>
                </div>
                <div class="fun-custom-column">
                    <div class="single-fun-factor">
                        <div class="text-icon block">
                            <img src="../asset/img/icons/face.png" alt="" class="mr-15">
                            <h2><span class="counter">180</span></h2>
                        </div>
                        <h4>Happy Clients</h4>
                    </div>
                </div>
                <div class="fun-custom-column">
                    <div class="single-fun-factor">
                        <div class="text-icon block">
                            <img src="../asset/img/icons/teams.png" alt="" class="mr-15">
                            <h2><span class="counter">62</span></h2>
                        </div>
                        <h4>Team Member</h4>
                    </div>
                </div>
                <div class="fun-custom-column">
                    <div class="single-fun-factor">
                        <div class="text-icon block">
                            <img src="../asset/img/icons/trophy.png" alt="" class="mr-15">
                            <h2><span class="counter">45</span></h2>
                        </div>
                        <h4>Awards Win</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Fun Factor End -->
    <!-- Team Area Start -->
    <div class="team-area fix pt-120 pb-120">
        <div class="container">
            <div class="row">
                <div class="col-xl-8 offset-xl-2 col-lg-10 offset-lg-1">
                    <div class="section-title text-center mb-95">
                        <h2>Our Team</h2>
                        <p>Financo amet sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-3 col-lg-4 col-md-6 col-12">
                    <div class="single-team-member">
                        <div class="member-image">
                            <a href="#" class="block">
                                <img src="../asset/img/team/1.png" alt="">
                            </a>
                        </div>
                        <div class="member-text">
                            <h3><a href="#">Robert Williams</a></h3>
                            <span>Chef Executive Officer</span>
                            <p>Financo amet sit amet, constur adipiscing elit, sed do mod tempor incididunt ostrud </p>
                            <div class="link-effect">
                                <ul>
                                    <li>
                                        <a href="#" class="block p-img"><img src="../asset/img/icons/call.png" alt=""></a>
                                        <a href="#" class="block s-img"><img src="../asset/img/icons/call-hover.png" alt=""></a>
                                    </li>
                                    <li>
                                        <a href="#" class="block p-img"><img src="../asset/img/icons/facebook.png" alt=""></a>
                                        <a href="#" class="block s-img"><img src="../asset/img/icons/facebook-hover.png" alt=""></a>
                                    </li>
                                    <li>
                                        <a href="#" class="block p-img"><img src="../asset/img/icons/twitter.png" alt=""></a>
                                        <a href="#" class="block s-img"><img src="../asset/img/icons/twitter-hover.png" alt=""></a>
                                    </li>
                                    <li>
                                        <a href="#" class="block p-img"><img src="../asset/img/icons/google-plus.png" alt=""></a>
                                        <a href="#" class="block s-img"><img src="../asset/img/icons/google-plus-hover.png" alt=""></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-4 col-md-6 col-12">
                    <div class="single-team-member">
                        <div class="member-image">
                            <a href="#" class="block">
                                <img src="../asset/img/team/2.png" alt="">
                            </a>
                        </div>
                        <div class="member-text">
                            <h3><a href="#">Jasmin Jaquline</a></h3>
                            <span>Financial Advisor</span>
                            <p>Financo amet sit amet, constur adipiscing elit, sed do mod tempor incididunt ostrud </p>
                            <div class="link-effect">
                                <ul>
                                    <li>
                                        <a href="#" class="block p-img"><img src="../asset/img/icons/call.png" alt=""></a>
                                        <a href="#" class="block s-img"><img src="../asset/img/icons/call-hover.png" alt=""></a>
                                    </li>
                                    <li>
                                        <a href="#" class="block p-img"><img src="../asset/img/icons/facebook.png" alt=""></a>
                                        <a href="#" class="block s-img"><img src="../asset/img/icons/facebook-hover.png" alt=""></a>
                                    </li>
                                    <li>
                                        <a href="#" class="block p-img"><img src="../asset/img/icons/twitter.png" alt=""></a>
                                        <a href="#" class="block s-img"><img src="../asset/img/icons/twitter-hover.png" alt=""></a>
                                    </li>
                                    <li>
                                        <a href="#" class="block p-img"><img src="../asset/img/icons/google-plus.png" alt=""></a>
                                        <a href="#" class="block s-img"><img src="../asset/img/icons/google-plus-hover.png" alt=""></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-4 col-md-6 col-12">
                    <div class="single-team-member mt-sm-30">
                        <div class="member-image">
                            <a href="#" class="block">
                                <img src="../asset/img/team/3.png" alt="">
                            </a>
                        </div>
                        <div class="member-text">
                            <h3><a href="#">Sharlok Homes</a></h3>
                            <span>Chef Executive Officer</span>
                            <p>Financo amet sit amet, constur adipiscing elit, sed do mod tempor incididunt ostrud </p>
                            <div class="link-effect">
                                <ul>
                                    <li>
                                        <a href="#" class="block p-img"><img src="../asset/img/icons/call.png" alt=""></a>
                                        <a href="#" class="block s-img"><img src="../asset/img/icons/call-hover.png" alt=""></a>
                                    </li>
                                    <li>
                                        <a href="#" class="block p-img"><img src="../asset/img/icons/facebook.png" alt=""></a>
                                        <a href="#" class="block s-img"><img src="../asset/img/icons/facebook-hover.png" alt=""></a>
                                    </li>
                                    <li>
                                        <a href="#" class="block p-img"><img src="../asset/img/icons/twitter.png" alt=""></a>
                                        <a href="#" class="block s-img"><img src="../asset/img/icons/twitter-hover.png" alt=""></a>
                                    </li>
                                    <li>
                                        <a href="#" class="block p-img"><img src="../asset/img/icons/google-plus.png" alt=""></a>
                                        <a href="#" class="block s-img"><img src="../asset/img/icons/google-plus-hover.png" alt=""></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-4 col-md-6 col-12">
                    <div class="single-team-member mt-md-30 mt-sm-30">
                        <div class="member-image">
                            <a href="#" class="block">
                                <img src="../asset/img/team/4.png" alt="">
                            </a>
                        </div>
                        <div class="member-text">
                            <h3><a href="#">Rose Smith</a></h3>
                            <span>Financial Consultant</span>
                            <p>Financo amet sit amet, constur adipiscing elit, sed do mod tempor incididunt ostrud </p>
                            <div class="link-effect">
                                <ul>
                                    <li>
                                        <a href="#" class="block p-img"><img src="../asset/img/icons/call.png" alt=""></a>
                                        <a href="#" class="block s-img"><img src="../asset/img/icons/call-hover.png" alt=""></a>
                                    </li>
                                    <li>
                                        <a href="#" class="block p-img"><img src="../asset/img/icons/facebook.png" alt=""></a>
                                        <a href="#" class="block s-img"><img src="../asset/img/icons/facebook-hover.png" alt=""></a>
                                    </li>
                                    <li>
                                        <a href="#" class="block p-img"><img src="../asset/img/icons/twitter.png" alt=""></a>
                                        <a href="#" class="block s-img"><img src="../asset/img/icons/twitter-hover.png" alt=""></a>
                                    </li>
                                    <li>
                                        <a href="#" class="block p-img"><img src="../asset/img/icons/google-plus.png" alt=""></a>
                                        <a href="#" class="block s-img"><img src="../asset/img/icons/google-plus-hover.png" alt=""></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Team Area End -->
    <!-- testimonial Carousel Start -->
    <div class="testmonial-carousel bg-light ptb-70">
        <div class="container">
            <div class="row">
                <div class="col-xl-8 offset-xl-2 col-lg-10 offset-lg-1">
                    <div class="testmonial-wrapper text-center">
                        <div class="single-testi">
                            <div class="testi-img">
                                <img src="../asset/img/testi/1.jpg" alt="">
                            </div>
                            <div class="testi-text">
                                <p>Financo amet sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequatuis aute irure</p>
                                <h5><a href="#">Andrew Williams</a></h5>
                                <span>CEO &amp; Founder</span>
                            </div>
                        </div>
                        <div class="single-testi">
                            <div class="testi-img">
                                <img src="../asset/img/testi/1.jpg" alt="">
                            </div>
                            <div class="testi-text">
                                <p>Financo amet sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequatuis aute irure</p>
                                <h5><a href="#">Andrew Williams</a></h5>
                                <span>CEO &amp; Founder</span>
                            </div>
                        </div>
                        <div class="single-testi">
                            <div class="testi-img">
                                <img src="../asset/img/testi/1.jpg" alt="">
                            </div>
                            <div class="testi-text">
                                <p>Financo amet sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequatuis aute irure</p>
                                <h5><a href="#">Andrew Williams</a></h5>
                                <span>CEO &amp; Founder</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- testimonial Carousel End -->
    <!-- Blog Area Start -->
    <div class="blog-area pt-120 pb-115">
        <div class="container">
            <div class="row">
                <div class="col-xl-8 offset-xl-2 col-lg-10 offset-lg-1">
                    <div class="section-title text-center mb-55">
                        <h2>From Blog</h2>
                        <p>Financo amet sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco </p>
                    </div>
                </div>
            </div>
            <div class="blog-wrapper">
                <div class="col-12">
                    <div class="single-blog">
                        <div class="blog-image">
                            <a href="blog-details.html"><img src="../asset/img/blog/1.jpg" alt=""></a>
                            <span>10 January, 2017</span>
                        </div>
                        <div class="blog-text">
                            <h4><a href="blog-details.html">Financial Meeting</a></h4>
                            <p>Financo consectetur adipiscing elited do eiusmod tempor incididnt mint </p>
                            <a href="blog-details.html">Read More</a>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="single-blog">
                        <div class="blog-image">
                            <a href="blog-details.html"><img src="../asset/img/blog/2.jpg" alt=""></a>
                            <span>08 January, 2017</span>
                        </div>
                        <div class="blog-text">
                            <h4><a href="blog-details.html">Financial Planning</a></h4>
                            <p>Financo consectetur adipiscing elited do eiusmod tempor incididnt mint </p>
                            <a href="blog-details.html">Read More</a>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="single-blog">
                        <div class="blog-image">
                            <a href="blog-details.html"><img src="../asset/img/blog/3.jpg" alt=""></a>
                            <span>06 January, 2017</span>
                        </div>
                        <div class="blog-text">
                            <h4><a href="blog-details.html">Financial Tips and Tricks</a></h4>
                            <p>Financo consectetur adipiscing elited do eiusmod tempor incididnt mint </p>
                            <a href="blog-details.html">Read More</a>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="single-blog">
                        <div class="blog-image">
                            <a href="blog-details.html"><img src="../asset/img/blog/4.jpg" alt=""></a>
                            <span>04 January, 2017</span>
                        </div>
                        <div class="blog-text">
                            <h4><a href="blog-details.html">Financial Investment</a></h4>
                            <p>Financo consectetur adipiscing elited do eiusmod tempor incididnt mint </p>
                            <a href="blog-details.html">Read More</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Blog Area End -->
    <!--Start of Client area-->
    <div class="client-area ptb-40 bg-light">
        <div class="container">
            <div class="client-carousel">
                <div class="col-12">
                    <div class="single-client block">
                        <a href="#" class="block">
                            <span class="p-images"><img src="../asset/img/client/1.png" alt=""></span>
                            <span class="s-images"><img src="../asset/img/client/1-hover.png" alt=""></span>
                        </a>
                    </div>
                </div>
                <div class="col-12">
                    <div class="single-client block">
                        <a href="#" class="block">
                            <span class="p-images"><img src="../asset/img/client/2.png" alt=""></span>
                            <span class="s-images"><img src="../asset/img/client/2-hover.png" alt=""></span>
                        </a>
                    </div>
                </div>
                <div class="col-12">
                    <div class="single-client block">
                        <a href="#" class="block">
                            <span class="p-images"><img src="../asset/img/client/3.png" alt=""></span>
                            <span class="s-images"><img src="../asset/img/client/3-hover.png" alt=""></span>
                        </a>
                    </div>
                </div>
                <div class="col-12">
                    <div class="single-client block">
                        <a href="#" class="block">
                            <span class="p-images"><img src="../asset/img/client/4.png" alt=""></span>
                            <span class="s-images"><img src="../asset/img/client/4-hover.png" alt=""></span>
                        </a>
                    </div>
                </div>
                <div class="col-12">
                    <div class="single-client block">
                        <a href="#" class="block">
                            <span class="p-images"><img src="../asset/img/client/5.png" alt=""></span>
                            <span class="s-images"><img src="../asset/img/client/5-hover.png" alt=""></span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--End of Client area-->

    <?php include 'include/footer.php'; ?>

</body>

</html>