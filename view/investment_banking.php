<?php include 'config/includeWithVariables.php'; ?>


<!doctype html>
<html class="no-js" lang="en">

<?php includeWithVariables('view/include/script_header.php', array('title' => 'Investment Banking')); ?>

<body>
    <?php include 'include/header.php'; ?>


    <!-- breadcrumbs Area Start-->
    <div class="breadcrumbs-area bg-overlay-dark bg-9" id="paralax" style="background-image:linear-gradient(rgb(255 255 255 / 47%) 68%, rgb(31 32 32) 100%), url(<?php echo ASSET_URL ?>img/banner/14.jpg)">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumbs-text text-left">
                        <h2>NVESTMENT BANKING
                        </h2>
                        <div class="breadcrumbs-bar">
                            <ul class="breadcrumbs">
                                <li>Astar Investment Limited ARE KNOWN FOR WORKING WITH A DIVERSE RANGE OF ISSUER CLIENTS, INCLUDING INSTITUTIONS, GOVERNMENTS, AND CORPORATIONS.</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- breadcrumbs Area End -->

    <!-- <section class="section section-md bg-default novi-background"> -->
    <div class="container p-5">
        <div class="row justify-content-md-center ">

            <div class="col-md-8">
                <!-- Blurb circle-->
                <article class="blurb blurb-circle">
                    <div class="unit flex-sm-row unit-spacing-md">

                        <div class="unit__body">
                            <p>Ford Beckett understands that their greatest asset is their loyal client base. Within our investment banking team, our brokers work with a large number of issuer clients, including corporations, governments and institutions, and provides all of our clients with comprehensive strategic advice, capital raising and risk management expertise. We know that our client base is diverse, and their needs are just as broad, this means that we as a company need to have as many investment vehicles as possible to satisfy all our clients needs.</p>

                            <p>Here at Ford Beckett we all share a client first belief, and when making any decisions regarding their capital, or investments we always put the client first. Over the years we have built a solid reputation for our ability to show our clients the best possible service, handled with attention to detail, and a lot of care. Ford Beckett are proud of their ability to create, build and maintain long lasting business relationships with each and every one of our clients by ensuring that their needs come first, and their goals are met.</p>

                            <p>Ford Beckett are known for working with a diverse range of issuer clients, including institutions, governments, and corporations. Our team of investment bankers provide our clients with in depth strategies, advice risk management and mitigation services as well as capital raising. Our team are backed by our highly experienced research team of industry specialists, which enables us to serve our clients with the best possible choices, in today’s ever evolving marketplace.</p>

                            <p>We serve the ever evolving needs of our clients around the world across:</p>
                            <ul class="icon_list">
                                <li>Consumer &amp; Retail</li>
                                <li>Diversified Industries</li>
                                <li>Energy</li>
                                <li>Financial Institutions &amp; Governments</li>
                                <li>Financial Sponsors</li>
                                <li>Healthcare</li>
                                <li>Real Estate &amp; Lodging</li>
                                <li>Technology, Media &amp; Telecom</li>
                            </ul>
                        </div>
                    </div>
                </article>
            </div>


            <?php include 'include/market-update.php'; ?>

        </div>
    </div>
    <!-- </section> -->


    <?php include 'include/footer.php'; ?>

</body>

</html>