<?php include 'config/includeWithVariables.php'; ?>

<!doctype html>
<html class="no-js" lang="en">

<?php includeWithVariables('view/include/script_header.php', array('title' => 'Contact')); ?>



<body>
    <?php include 'include/header.php'; ?>

    <!-- breadcrumbs Area Start-->
    <div class="breadcrumbs-area bg-overlay-dark bg-6" id="paralax" style="background-image:linear-gradient(rgb(255 255 255 / 47%) 68%, rgb(31 32 32) 100%), url(<?php echo ASSET_URL ?>img/banner/11.jpg)">
        <div class=" container">
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumbs-text text-left">
                        <h2>CONTACT US</h2>
                        <div class="breadcrumbs-bar">
                            <ul class="breadcrumbs">
                                <li><a href="index.html">HOME</a></li>
                                <li>CONTACT</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- breadcrumbs Area End -->
    <!--Start of Google Map-->
    <div class="google-map-area ">
        <div class="container">
            <div class="row">
                <div class="col-lg-10 col-lg-offset-1 col-md-12">
                    <div class="contact-information">
                        <div class="single-contact-info">
                            <div class="contents">
                                <img src="../asset/img/icons/c-map.png" alt="">
                                <div class="info-text">
                                    <span class="block">One Hennessy, Level 12, 1 Hennessy Road, Wan Chai, Hong Kong, China S.A.R</span>
                                </div>
                            </div>
                        </div>
                        <div class="single-contact-info">
                            <div class="contents">
                                <img src="../asset/img/icons/c-phone.png" alt="">
                                <div class="info-text">
                                    <span class="block">Telephone : +852 3018 3094</span>
                                    <span class="block">Telephone : +852 3016 2332</span>
                                </div>
                            </div>
                        </div>
                        <div class="single-contact-info">
                            <div class="contents">
                                <img src="../asset/img/icons/c-globe.png" alt="">
                                <div class="info-text">
                                    <a href="mailto:info@fordbeckett.com" style="color: black!important;"> <span class="block">Email : info@fordbeckett.com</span></a>
                                    <!-- <span class="block">Web : www.devitems.com</span> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--End of Google Map-->
    <!--Start Contact Form Area-->
    <div class="contact-form-area fix pb-120">
        <div class="container">
            <div class="contact-form-left">
                <h4 class="details-title">Leave a Message</h4>
                <form id="contact-form" action="view/send_email.php" method="post">
                    <input type="text" name="name" placeholder="Your name" required>
                    <input type="email" name="email" placeholder="Email here" required>
                    <input type="text" name="subject" placeholder="Subject here" required>
                    <textarea name="message" cols="30" rows="10" placeholder="Write here" required></textarea>
                    <div class="w-100 fix">
                        <button type="submit" id="kategori" class="default-button submit-btn">SUBMIT</button>
                    </div>
                    <p class="form-messege"></p>
                </form>


            </div>
            <div class="contact-form-right">
                <div class="single-sidebar-widget fix bg-8">
                    <div class="sidebar-contact-info">
                        <h1><?php echo $_COOKIE['send_email'] ?></h1>
                        <p>Financo consectetur adipiscing elitsed do eiud tempor incididnt in the nto sed do eiusmod tempor incididunt ut labore et dolore magna aliqua enim </p>
                        <a href="contact.html" class="default-button">Contact</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--End of Contact Form Area-->

    <?php include 'include/footer.php'; ?>

    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.4.0/dist/sweetalert2.all.min.js"></script>

    <?php
    if (isset($_COOKIE['send_email']) && $_COOKIE['send_email'] == "berhasil") {
    ?>
        <script type="text/javascript">
            console.log('clickkk')

            window.onload = function() {
                Swal.fire({
                    title: 'Success!',
                    text: 'Your messages succesfulyy send',
                    icon: 'success',
                    confirmButtonText: 'Cool'
                })
                // $('#kategori').on('click', function() {

                // });
            };
        </script>
    <?php

    } else if (isset($_COOKIE['send_email']) && $_COOKIE['send_email'] == "gagal") {
    ?>
        <script type="text/javascript">
            console.log('clickkk')

            window.onload = function() {
                Swal.fire({
                    title: 'Error!',
                    text: 'Somthing wrong',
                    icon: 'error',
                    confirmButtonText: 'Cool'
                })
                // $('#kategori').on('click', function() {

                // });
            };
        </script>
    <?php
    }
    ?>




</body>

</html>