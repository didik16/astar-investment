<?php include 'config/includeWithVariables.php'; ?>


<!doctype html>
<html class="no-js" lang="en">

<?php includeWithVariables('view/include/script_header.php', array('title' => 'Research')); ?>

<body>
    <?php include 'include/header.php'; ?>


    <!-- breadcrumbs Area Start-->
    <div class="breadcrumbs-area bg-overlay-dark bg-9" id="paralax" style="background-image:linear-gradient(rgb(255 255 255 / 47%) 68%, rgb(31 32 32) 100%), url(<?php echo ASSET_URL ?>img/banner/9.jpg)">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumbs-text text-left">
                        <h2>OUR TEAM</h2>
                        <div class="breadcrumbs-bar">
                            <ul class="breadcrumbs">

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- breadcrumbs Area End -->

    <!-- <section class="section section-md bg-default novi-background"> -->
    <div class="container p-5">
        <div class="row justify-content-md-center ">

            <div class="col-md-12">
                <!-- Blurb circle-->
                <div class="team-area fix pb-120">
                    <div class="container">

                        <div class="row">
                            <!-- <div class="section-title text-center mb-95">
                                <h2>Our Team</h2>
                                <p>Financo amet sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco </p>
                            </div> -->
                            <div class="col-xl-3 col-lg-4 col-md-6 col-12">
                                <div class="single-team-member">
                                    <div class="member-image">
                                        <a href="#" class="block">
                                            <img src="<?php echo ASSET_URL ?>img/team/1.png" alt="">
                                        </a>
                                    </div>
                                    <div class="member-text">

                                        <h3><a href="#">Robert Williams</a></h3>
                                        <span>Chef Executive Officer</span>
                                        <p>Financo amet sit amet, constur adipiscing elit, sed do mod tempor incididunt ostrud </p>
                                        <div class="link-effect">
                                            <ul>
                                                <li>
                                                    <a href="#" class="block p-img"><img src="<?php echo ASSET_URL ?>img/icons/call.png" alt=""></a>
                                                    <a href="#" class="block s-img"><img src="<?php echo ASSET_URL ?>img/icons/call-hover.png" alt=""></a>
                                                </li>
                                                <li>
                                                    <a href="#" class="block p-img"><img src="<?php echo ASSET_URL ?>img/icons/facebook.png" alt=""></a>
                                                    <a href="#" class="block s-img"><img src="<?php echo ASSET_URL ?>img/icons/facebook-hover.png" alt=""></a>
                                                </li>
                                                <li>
                                                    <a href="#" class="block p-img"><img src="<?php echo ASSET_URL ?>img/icons/twitter.png" alt=""></a>
                                                    <a href="#" class="block s-img"><img src="<?php echo ASSET_URL ?>img/icons/twitter-hover.png" alt=""></a>
                                                </li>
                                                <li>
                                                    <a href="#" class="block p-img"><img src="<?php echo ASSET_URL ?>img/icons/google-plus.png" alt=""></a>
                                                    <a href="#" class="block s-img"><img src="<?php echo ASSET_URL ?>img/icons/google-plus-hover.png" alt=""></a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-4 col-md-6 col-12">
                                <div class="single-team-member">
                                    <div class="member-image">
                                        <a href="#" class="block">
                                            <img src="<?php echo ASSET_URL ?>img/team/2.png" alt="">
                                        </a>
                                    </div>
                                    <div class="member-text">
                                        <h3><a href="#">Jasmin Jaquline</a></h3>
                                        <span>Financial Advisor</span>
                                        <p>Financo amet sit amet, constur adipiscing elit, sed do mod tempor incididunt ostrud </p>
                                        <div class="link-effect">
                                            <ul>
                                                <li>
                                                    <a href="#" class="block p-img"><img src="<?php echo ASSET_URL ?>img/icons/call.png" alt=""></a>
                                                    <a href="#" class="block s-img"><img src="<?php echo ASSET_URL ?>img/icons/call-hover.png" alt=""></a>
                                                </li>
                                                <li>
                                                    <a href="#" class="block p-img"><img src="<?php echo ASSET_URL ?>img/icons/facebook.png" alt=""></a>
                                                    <a href="#" class="block s-img"><img src="<?php echo ASSET_URL ?>img/icons/facebook-hover.png" alt=""></a>
                                                </li>
                                                <li>
                                                    <a href="#" class="block p-img"><img src="<?php echo ASSET_URL ?>img/icons/twitter.png" alt=""></a>
                                                    <a href="#" class="block s-img"><img src="<?php echo ASSET_URL ?>img/icons/twitter-hover.png" alt=""></a>
                                                </li>
                                                <li>
                                                    <a href="#" class="block p-img"><img src="<?php echo ASSET_URL ?>img/icons/google-plus.png" alt=""></a>
                                                    <a href="#" class="block s-img"><img src="<?php echo ASSET_URL ?>img/icons/google-plus-hover.png" alt=""></a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-4 col-md-6 col-12">
                                <div class="single-team-member mt-sm-30">
                                    <div class="member-image">
                                        <a href="#" class="block">
                                            <img src="<?php echo ASSET_URL ?>img/team/3.png" alt="">
                                        </a>
                                    </div>
                                    <div class="member-text">
                                        <h3><a href="#">Sharlok Homes</a></h3>
                                        <span>Chef Executive Officer</span>
                                        <p>Financo amet sit amet, constur adipiscing elit, sed do mod tempor incididunt ostrud </p>
                                        <div class="link-effect">
                                            <ul>
                                                <li>
                                                    <a href="#" class="block p-img"><img src="<?php echo ASSET_URL ?>img/icons/call.png" alt=""></a>
                                                    <a href="#" class="block s-img"><img src="<?php echo ASSET_URL ?>img/icons/call-hover.png" alt=""></a>
                                                </li>
                                                <li>
                                                    <a href="#" class="block p-img"><img src="<?php echo ASSET_URL ?>img/icons/facebook.png" alt=""></a>
                                                    <a href="#" class="block s-img"><img src="<?php echo ASSET_URL ?>img/icons/facebook-hover.png" alt=""></a>
                                                </li>
                                                <li>
                                                    <a href="#" class="block p-img"><img src="<?php echo ASSET_URL ?>img/icons/twitter.png" alt=""></a>
                                                    <a href="#" class="block s-img"><img src="<?php echo ASSET_URL ?>img/icons/twitter-hover.png" alt=""></a>
                                                </li>
                                                <li>
                                                    <a href="#" class="block p-img"><img src="<?php echo ASSET_URL ?>img/icons/google-plus.png" alt=""></a>
                                                    <a href="#" class="block s-img"><img src="<?php echo ASSET_URL ?>img/icons/google-plus-hover.png" alt=""></a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-4 col-md-6 col-12">
                                <div class="single-team-member mt-md-30 mt-sm-30">
                                    <div class="member-image">
                                        <a href="#" class="block">
                                            <img src="<?php echo ASSET_URL ?>img/team/4.png" alt="">
                                        </a>
                                    </div>
                                    <div class="member-text">
                                        <h3><a href="#">Rose Smith</a></h3>
                                        <span>Financial Consultant</span>
                                        <p>Financo amet sit amet, constur adipiscing elit, sed do mod tempor incididunt ostrud </p>
                                        <div class="link-effect">
                                            <ul>
                                                <li>
                                                    <a href="#" class="block p-img"><img src="<?php echo ASSET_URL ?>img/icons/call.png" alt=""></a>
                                                    <a href="#" class="block s-img"><img src="<?php echo ASSET_URL ?>img/icons/call-hover.png" alt=""></a>
                                                </li>
                                                <li>
                                                    <a href="#" class="block p-img"><img src="<?php echo ASSET_URL ?>img/icons/facebook.png" alt=""></a>
                                                    <a href="#" class="block s-img"><img src="<?php echo ASSET_URL ?>img/icons/facebook-hover.png" alt=""></a>
                                                </li>
                                                <li>
                                                    <a href="#" class="block p-img"><img src="<?php echo ASSET_URL ?>img/icons/twitter.png" alt=""></a>
                                                    <a href="#" class="block s-img"><img src="<?php echo ASSET_URL ?>img/icons/twitter-hover.png" alt=""></a>
                                                </li>
                                                <li>
                                                    <a href="#" class="block p-img"><img src="<?php echo ASSET_URL ?>img/icons/google-plus.png" alt=""></a>
                                                    <a href="#" class="block s-img"><img src="<?php echo ASSET_URL ?>img/icons/google-plus-hover.png" alt=""></a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>


        </div>
    </div>
    <!-- </section> -->


    <?php include 'include/footer.php'; ?>

</body>

</html>