<?php include 'config/includeWithVariables.php'; ?>
<?php include 'config/base_url.php'; ?>

<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Astar Investment Limited</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Favicon Icon -->
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo ASSET_URL ?>img/favicon.ico">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,600" rel="stylesheet">

    <!-- All css here -->
    <link rel="stylesheet" href="<?php echo ASSET_URL ?>css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo ASSET_URL ?>css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo ASSET_URL ?>css/shortcode/shortcodes.css">
    <link rel="stylesheet" href="<?php echo ASSET_URL ?>css/animate.css">
    <link rel="stylesheet" href="<?php echo ASSET_URL ?>css/owl.carousel.css">
    <link rel="stylesheet" href="<?php echo ASSET_URL ?>css/style.css">
    <link rel="stylesheet" href="<?php echo ASSET_URL ?>css/responsive.css">

</head>

<body>
    <?php include 'include/header.php'; ?>

    <!-- Background Area Start -->
    <div class="slider-four-area">
        <div class="slider-wrapper">
            <div class="single-slide" style="background-image: linear-gradient(rgb(0 0 0 / 47%) 68%, rgb(31, 32, 32) 100%), url('<?php echo ASSET_URL ?>img/slider/1.jpg');">
                <div class="banner-content">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-10 offset-lg-1">
                                <div class="text-content-wrapper">
                                    <div class="text-content text-center">
                                        <!-- <h3 class="title1 pt-65">WE PROVIDE BEST FINANCIAL SOLUTIONS</h3> -->
                                        <h1 class="title2">BUILD A LEGACY</h1>
                                        <p>Astar Investment Limited WILL ENSURE YOUR FAMILY’S FINANCIAL FUTURE</p>
                                        <div class="banner-readmore">
                                            <a class="button banner-btn" href="#">VIEW SERVICES</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="single-slide" style="background-image: linear-gradient(rgb(0 0 0 / 47%) 68%, rgb(31, 32, 32) 100%), url('<?php echo ASSET_URL ?>img/slider/4.jpg');">
                <div class="banner-content">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-10 offset-lg-1">
                                <div class="text-content-wrapper slide-two">
                                    <div class="text-content text-center">
                                        <h3 class="title1 pt-65">WE PROVIDE BEST FINANCIAL SOLUTIONS</h3>
                                        <h1 class="title2">FOR YOUR FINANCIAL PLAN</h1>
                                        <p>We have over 25 year’s of experience in Finance and Business management so we can make your business more successful you can trust us</p>
                                        <div class="banner-readmore">
                                            <a class="button banner-btn" href="#">VIEW SERVICES</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="single-slide" style="background-image: linear-gradient(rgb(0 0 0 / 47%) 68%, rgb(31, 32, 32) 100%), url('<?php echo ASSET_URL ?>img/slider/2.jpg');">
                <div class="banner-content">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-10 offset-lg-1">
                                <div class="text-content-wrapper slide-two">
                                    <div class="text-content text-center">
                                        <!-- <h3 class="title1 pt-65">WE PROVIDE BEST FINANCIAL SOLUTIONS</h3> -->
                                        <h1 class="title2">ENTERING THE MARKETS MADE SIMPLE</h1>
                                        <p>CONTACT OUR TEAM TODAY TO SEE JUST HOW SIMPLE IT IS TO BEGIN
                                            ON YOUR PATH TO FINANCIAL SECURITY</p>
                                        <div class="banner-readmore">
                                            <a class="button banner-btn" href="#">VIEW SERVICES</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="single-slide" style="background-image: linear-gradient(rgb(0 0 0 / 47%) 68%, rgb(31, 32, 32) 100%), url('<?php echo ASSET_URL ?>img/slider/3.jpg');">
                <div class="banner-content">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-10 offset-lg-1">
                                <div class="text-content-wrapper slide-two">
                                    <div class="text-content text-center">
                                        <!-- <h3 class="title1 pt-65">WE PROVIDE BEST FINANCIAL SOLUTIONS</h3> -->
                                        <h1 class="title2">DEDICATED FINANCIAL ADVISORS</h1>
                                        <p>HERE TO HELP YOU ACHIEVE YOUR GOALS</p>
                                        <div class="banner-readmore">
                                            <a class="button banner-btn" href="#">VIEW SERVICES</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Background Area End -->

    <!-- Services Area Start -->
    <div class="service-main-area-2 pt-100 pb-105 bg-rotate-2">
        <div class="service-banner">
            <div class="container">
                <div class="service-banner-left">
                    <img src="<?php echo ASSET_URL ?>img/banner/3.jpg" alt="">
                </div>
                <div class="service-banner-right">
                    <div class="information-text">
                        <h3><span>ABOUT </span></h3>
                        <h2>Astar Investment Limited</h2>
                        <p>Here at Astar Investment Limited, we have a team of highly experienced investment managers that are well versed in an large number of wealth management services. We tailor our investment portfolio services to each of our clients according to their priorities, goals, preferred time horizon and risk tolerance. Although each client has a unique financial situation, the common goal for our Portfolio managers is to create and safeguard wealth,while providing the best options to manage tax obligations related to their investments. Our services include advisory management, discretional management and institutional portfolio management in addition to management services for professional intermediaries.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="service-area pt-105">
            <div class="container">

                <div class="row mt-5">
                    <div class="col-lg-3 col-md-6">
                        <div class="single-item">
                            <div class="service-item-image">
                                <img src="<?php echo ASSET_URL ?>img/service/1.jpg" alt="">
                                <a href="services.html" class="default-button">Learn More</a>
                            </div>
                            <div class="service-text text-center">
                                <h5><a href="services.html">Our Vision and Values</a></h5>
                                <!-- <p>Financo consectetur adipiscing elit, sed do iuidnt eiusmod tempor incididunt ut labore et dolore </p> -->
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="single-item">
                            <div class="service-item-image">
                                <img src="<?php echo ASSET_URL ?>img/service/3.jpg" alt="">
                                <a href="<?php echo BASE_URL ?>why_us" class="default-button">Learn More</a>
                            </div>
                            <div class="service-text text-center">
                                <h5><a href="<?php echo BASE_URL ?>why_us">Why Us</a></h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="single-item">
                            <div class="service-item-image">
                                <img src="<?php echo ASSET_URL ?>img/service/2.jpg" alt="">
                                <a href="services.html" class="default-button">Learn More</a>
                            </div>
                            <div class="service-text text-center">
                                <h5><a href="services.html">Investment Fees and Pricing</a></h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="single-item">
                            <div class="service-item-image">
                                <img src="<?php echo ASSET_URL ?>img/service/4.jpg" alt="">
                                <a href="<?php echo BASE_URL ?>career" class="default-button">Learn More</a>
                            </div>
                            <div class="service-text text-center">
                                <h5><a href="<?php echo BASE_URL ?>career">Careers</a></h5>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- Services Area End -->

    <!-- Services Area Start -->
    <div class="service-area  pb-105">
        <div class="container">
            <div class="row">
                <div class="col-xl-8 offset-xl-2 col-lg-10 offset-lg-1">
                    <div class="section-title text-center mb-55">
                        <h2>Our Services</h2>
                        <p>Astar Investment Limited amet sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-6">
                    <div class="single-item">
                        <span class="service-image">
                            <img src="<?php echo ASSET_URL ?>img/icons/trading_stock.png" alt="">
                        </span>
                        <div class="service-text">
                            <h5>TRADING STOCKS</h5>
                            <p>Astar Investment Limited connects our clients to the global financial market place, and allows them access to a number of stock opportunities…</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="single-item">
                        <span class="service-image">
                            <img src="<?php echo ASSET_URL ?>img/icons/cash.png" alt="">
                        </span>
                        <div class="service-text">
                            <h5>TRADING OPTIONS</h5>
                            <p>Our Options and derivative trading team specializes in giving our clients risk reducing strategies to implement in their...</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="single-item">
                        <span class="service-image">
                            <img src="<?php echo ASSET_URL ?>img/icons/eft.png" alt="">
                        </span>
                        <div class="service-text">
                            <h5>EFT TRADING</h5>
                            <p>ETF’s or Exchange Traded Funds are index funds that both list and trade on exchanges in the same way as stocks...</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="single-item">
                        <span class="service-image">
                            <img src="<?php echo ASSET_URL ?>img/icons/hand.png" alt="">
                        </span>
                        <div class="service-text">
                            <h5>FIXED INCOME TRADING</h5>
                            <p>Astar Investment Limited’s fixed income trading team is made up of a diverse group of financial professionals who between them...</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="single-item">
                        <span class="service-image">
                            <img src="<?php echo ASSET_URL ?>img/icons/commo.png" alt="">
                        </span>
                        <div class="service-text">
                            <h5>INVESTMENT CHOICES</h5>
                            <p> Creating a unique tailor made portfolio is not an easy task to perform by yourself. Astar Investment Limited understands this and will work with you to create…</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="single-item">
                        <span class="service-image">
                            <img src="<?php echo ASSET_URL ?>img/icons/reti.png" alt="">
                        </span>
                        <div class="service-text">
                            <h5>INSTITUTIONAL TRADING</h5>
                            <p>Astar Investment Limited’s team of industry leading institutional traders is comprised of some of the most experienced and well respected minds...</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="single-item">
                        <span class="service-image">
                            <img src="<?php echo ASSET_URL ?>img/icons/brif.png" alt="">
                        </span>
                        <div class="service-text">
                            <h5>PORTFOLIO MANAGEMENT</h5>
                            <p>Astar Investment Limited have always believed in assisting and advising their clients to the fullest with each of the services they offer...</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="single-item">
                        <span class="service-image">
                            <img src="<?php echo ASSET_URL ?>img/icons/wealth.png" alt="">
                        </span>
                        <div class="service-text">
                            <h5>WEALTH MANAGEMENT</h5>
                            <p>Our brokers are always looking at ways to better a client’s portfolio and to do this they go above and beyond to get to know you, and your financial goals...</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="single-item">
                        <span class="service-image">
                            <img src="<?php echo ASSET_URL ?>img/icons/merger.png" alt="">
                        </span>
                        <div class="service-text">
                            <h5>MERGERS</h5>
                            <p>Astar Investment Limited are proud to be able to meet and exceed our client’s expectations, and will strive to meet your short, mid and long-term goals...</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Services Area End -->

    <!-- Features Area Start -->
    <div class="features-area bg-light fix">
        <div class="feature-left-column">
            <img src="<?php echo ASSET_URL ?>img/banner/2.jpg" alt="">
        </div>
        <div class="feature-right-column">
            <div class="coustom-col-10">
                <div class="feature-text">
                    <h3><span>Why You</span></h3>
                    <h2>Choose Astar Investment Limited</h2>
                    <p>Astar Investment Limited since its foundation have consistently believed in a client first approach to investments. We believe that every individual investor no matter how experienced in the markets should have the same level of information when it comes to making decisions with their finances. We do not give our clients differing information based on their level of investment, or investment knowledge, we believe that every investor should have access to the best possible research and analysis when trading in the markets.</p>
                </div>
            </div>
            <div class="custom-row">
                <div class="coustom-col-5">
                    <div class="single-item">
                        <span class="service-image">
                            <img src="<?php echo ASSET_URL ?>img/icons/rocket.png" alt="">
                        </span>
                        <div class="service-text">
                            <h4>Fast Loan Approval</h4>
                            <p>Astar Investment Limited consectetur adipiscing elit, sed do eiusmod tempor incididnt </p>
                        </div>
                    </div>
                </div>
                <div class="coustom-col-5">
                    <div class="single-item">
                        <span class="service-image">
                            <img src="<?php echo ASSET_URL ?>img/icons/team.png" alt="">
                        </span>
                        <div class="service-text">
                            <h4>Dedicated Team</h4>
                            <p>Astar Investment Limited consectetur adipiscing elit, sed do eiusmod tempor incididnt </p>
                        </div>
                    </div>
                </div>
                <div class="coustom-col-5">
                    <div class="single-item">
                        <span class="service-image">
                            <img src="<?php echo ASSET_URL ?>img/icons/doc.png" alt="">
                        </span>
                        <div class="service-text">
                            <h4>Free Documentation</h4>
                            <p>Astar Investment Limited consectetur adipiscing elit, sed do eiusmod tempor incididnt </p>
                        </div>
                    </div>
                </div>
                <div class="coustom-col-5">
                    <div class="single-item">
                        <span class="service-image">
                            <img src="<?php echo ASSET_URL ?>img/icons/refi.png" alt="">
                        </span>
                        <div class="service-text">
                            <h4>Refinancing</h4>
                            <p>Astar Investment Limited consectetur adipiscing elit, sed do eiusmod tempor incididnt </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Features Area End -->
    <!-- Advertise Area Start -->
    <div class="advertise-area bg-2 overlay-green">
        <div class="container">
            <div class="row">
                <div class="col-xl-8 offset-xl-2 col-lg-10 offset-lg-1">
                    <div class="advertise-text text-white text-center">
                        <h2>CONTACT US TODAY TO START YOUR JOURNEY TO FINANCIAL INDEPENDENCE</h2>
                        <p>We have over 25 year’s of experience in Finance and Business management so we can make your business more successful you can trust us and we care about it</p>
                        <a href="<?php echo BASE_URL ?>contact" class="default-button btn-white">CONTACT US</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Advertise Area End -->
    <!-- Projects Area Start -->
    <div class="projects-area ptb-120">
        <div class="container">
            <div class="row">
                <div class="col-xl-8 offset-xl-2 col-lg-10 offset-lg-1">
                    <div class="section-title text-center mb-55">
                        <h2>Our Projects</h2>
                        <p>Astar Investment Limited amet sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row custom-row grid fix">
                <div class="col-lg-4 col-md-6 col-12 grid-item">
                    <div class="project-style">
                        <div class="project-image">
                            <img src="<?php echo ASSET_URL ?>img/project/13.jpg" alt="" />
                        </div>
                        <div class="project-hover">
                            <a class="project-icon image-popup-no-margins" href="<?php echo ASSET_URL ?>img/project/13.jpg"><img src="<?php echo ASSET_URL ?>img/icons/src.png" alt=""></a>
                            <div class="project-text">
                                <h4><a href="project.html">Financial Planning</a></h4>
                                <p>Astar Investment Limited consectetur adipiscing elit, sed do eiusmod tempor incididnt </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-12 grid-item">
                    <div class="project-style">
                        <div class="project-image">
                            <img src="<?php echo ASSET_URL ?>img/project/14.jpg" alt="" />
                        </div>
                        <div class="project-hover">
                            <a class="project-icon image-popup-no-margins" href="<?php echo ASSET_URL ?>img/project/14.jpg"><img src="<?php echo ASSET_URL ?>img/icons/src.png" alt=""></a>
                            <div class="project-text">
                                <h4><a href="project.html">Financial Planning</a></h4>
                                <p>Astar Investment Limited consectetur adipiscing elit, sed do eiusmod tempor incididnt </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-12 grid-item">
                    <div class="project-style">
                        <div class="project-image">
                            <img src="<?php echo ASSET_URL ?>img/project/15.jpg" alt="" />
                        </div>
                        <div class="project-hover">
                            <a class="project-icon image-popup-no-margins" href="<?php echo ASSET_URL ?>img/project/15.jpg"><img src="<?php echo ASSET_URL ?>img/icons/src.png" alt=""></a>
                            <div class="project-text">
                                <h4><a href="project.html">Financial Planning</a></h4>
                                <p>Astar Investment Limited consectetur adipiscing elit, sed do eiusmod tempor incididnt </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-12 grid-item">
                    <div class="project-style">
                        <div class="project-image">
                            <img src="<?php echo ASSET_URL ?>img/project/16.jpg" alt="" />
                        </div>
                        <div class="project-hover">
                            <a class="project-icon image-popup-no-margins" href="<?php echo ASSET_URL ?>img/project/16.jpg"><img src="<?php echo ASSET_URL ?>img/icons/src.png" alt=""></a>
                            <div class="project-text">
                                <h4><a href="project.html">Financial Planning</a></h4>
                                <p>Astar Investment Limited consectetur adipiscing elit, sed do eiusmod tempor incididnt </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-12 grid-item">
                    <div class="project-style">
                        <div class="project-image">
                            <img src="<?php echo ASSET_URL ?>img/project/17.jpg" alt="" />
                        </div>
                        <div class="project-hover">
                            <a class="project-icon image-popup-no-margins" href="<?php echo ASSET_URL ?>img/project/17.jpg"><img src="<?php echo ASSET_URL ?>img/icons/src.png" alt=""></a>
                            <div class="project-text">
                                <h4><a href="project.html">Financial Planning</a></h4>
                                <p>Astar Investment Limited consectetur adipiscing elit, sed do eiusmod tempor incididnt </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-12 grid-item">
                    <div class="project-style">
                        <div class="project-image">
                            <img src="<?php echo ASSET_URL ?>img/project/18.jpg" alt="" />
                        </div>
                        <div class="project-hover">
                            <a class="project-icon image-popup-no-margins" href="<?php echo ASSET_URL ?>img/project/18.jpg"><img src="<?php echo ASSET_URL ?>img/icons/src.png" alt=""></a>
                            <div class="project-text">
                                <h4><a href="project.html">Financial Planning</a></h4>
                                <p>Astar Investment Limited consectetur adipiscing elit, sed do eiusmod tempor incididnt </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Projects Area End -->

    <!-- Fun Factor Start -->
    <!-- <div class="fun-factor-area bg-3 overlay-green pt-100 fix pb-95">
        <div class="container">
            <div class="fun-custom-row">
                <div class="fun-custom-column">
                    <div class="single-fun-factor">
                        <div class="text-icon block">
                            <img src="<?php echo ASSET_URL ?>img/icons/cup.png" alt="" class="mr-15">
                            <h2><span class="counter">750</span></h2>
                        </div>
                        <h4>Cup Of Coffee</h4>
                    </div>
                </div>
                <div class="fun-custom-column">
                    <div class="single-fun-factor">
                        <div class="text-icon block">
                            <img src="<?php echo ASSET_URL ?>img/icons/check.png" alt="" class="mr-15">
                            <h2><span class="counter">350</span></h2>
                        </div>
                        <h4>Case Complete</h4>
                    </div>
                </div>
                <div class="fun-custom-column">
                    <div class="single-fun-factor">
                        <div class="text-icon block">
                            <img src="<?php echo ASSET_URL ?>img/icons/face.png" alt="" class="mr-15">
                            <h2><span class="counter">180</span></h2>
                        </div>
                        <h4>Happy Clients</h4>
                    </div>
                </div>
                <div class="fun-custom-column">
                    <div class="single-fun-factor">
                        <div class="text-icon block">
                            <img src="<?php echo ASSET_URL ?>img/icons/teams.png" alt="" class="mr-15">
                            <h2><span class="counter">62</span></h2>
                        </div>
                        <h4>Team Member</h4>
                    </div>
                </div>
                <div class="fun-custom-column">
                    <div class="single-fun-factor">
                        <div class="text-icon block">
                            <img src="<?php echo ASSET_URL ?>img/icons/trophy.png" alt="" class="mr-15">
                            <h2><span class="counter">45</span></h2>
                        </div>
                        <h4>Awards Win</h4>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <!-- Fun Factor End -->
    <!-- Team Area Start -->
    <!-- <div class="team-area fix pt-120 pb-120">
        <div class="container">
            <div class="row">
                <div class="col-xl-8 offset-xl-2 col-lg-10 offset-lg-1">
                    <div class="section-title text-center mb-95">
                        <h2>Our Team</h2>
                        <p>Astar Investment Limited amet sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-3 col-lg-4 col-md-6 col-12">
                    <div class="single-team-member">
                        <div class="member-image">
                            <a href="#" class="block">
                                <img src="<?php echo ASSET_URL ?>img/team/1.png" alt="">
                            </a>
                        </div>
                        <div class="member-text">
                            <h3><a href="#">Robert Williams</a></h3>
                            <span>Chef Executive Officer</span>
                            <p>Astar Investment Limited amet sit amet, constur adipiscing elit, sed do mod tempor incididunt ostrud </p>
                            <div class="link-effect">
                                <ul>
                                    <li>
                                        <a href="#" class="block p-img"><img src="<?php echo ASSET_URL ?>img/icons/call.png" alt=""></a>
                                        <a href="#" class="block s-img"><img src="<?php echo ASSET_URL ?>img/icons/call-hover.png" alt=""></a>
                                    </li>
                                    <li>
                                        <a href="#" class="block p-img"><img src="<?php echo ASSET_URL ?>img/icons/facebook.png" alt=""></a>
                                        <a href="#" class="block s-img"><img src="<?php echo ASSET_URL ?>img/icons/facebook-hover.png" alt=""></a>
                                    </li>
                                    <li>
                                        <a href="#" class="block p-img"><img src="<?php echo ASSET_URL ?>img/icons/twitter.png" alt=""></a>
                                        <a href="#" class="block s-img"><img src="<?php echo ASSET_URL ?>img/icons/twitter-hover.png" alt=""></a>
                                    </li>
                                    <li>
                                        <a href="#" class="block p-img"><img src="<?php echo ASSET_URL ?>img/icons/google-plus.png" alt=""></a>
                                        <a href="#" class="block s-img"><img src="<?php echo ASSET_URL ?>img/icons/google-plus-hover.png" alt=""></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-4 col-md-6 col-12">
                    <div class="single-team-member">
                        <div class="member-image">
                            <a href="#" class="block">
                                <img src="<?php echo ASSET_URL ?>img/team/2.png" alt="">
                            </a>
                        </div>
                        <div class="member-text">
                            <h3><a href="#">Jasmin Jaquline</a></h3>
                            <span>Financial Advisor</span>
                            <p>Astar Investment Limited amet sit amet, constur adipiscing elit, sed do mod tempor incididunt ostrud </p>
                            <div class="link-effect">
                                <ul>
                                    <li>
                                        <a href="#" class="block p-img"><img src="<?php echo ASSET_URL ?>img/icons/call.png" alt=""></a>
                                        <a href="#" class="block s-img"><img src="<?php echo ASSET_URL ?>img/icons/call-hover.png" alt=""></a>
                                    </li>
                                    <li>
                                        <a href="#" class="block p-img"><img src="<?php echo ASSET_URL ?>img/icons/facebook.png" alt=""></a>
                                        <a href="#" class="block s-img"><img src="<?php echo ASSET_URL ?>img/icons/facebook-hover.png" alt=""></a>
                                    </li>
                                    <li>
                                        <a href="#" class="block p-img"><img src="<?php echo ASSET_URL ?>img/icons/twitter.png" alt=""></a>
                                        <a href="#" class="block s-img"><img src="<?php echo ASSET_URL ?>img/icons/twitter-hover.png" alt=""></a>
                                    </li>
                                    <li>
                                        <a href="#" class="block p-img"><img src="<?php echo ASSET_URL ?>img/icons/google-plus.png" alt=""></a>
                                        <a href="#" class="block s-img"><img src="<?php echo ASSET_URL ?>img/icons/google-plus-hover.png" alt=""></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-4 col-md-6 col-12">
                    <div class="single-team-member mt-sm-30">
                        <div class="member-image">
                            <a href="#" class="block">
                                <img src="<?php echo ASSET_URL ?>img/team/3.png" alt="">
                            </a>
                        </div>
                        <div class="member-text">
                            <h3><a href="#">Sharlok Homes</a></h3>
                            <span>Chef Executive Officer</span>
                            <p>Astar Investment Limited amet sit amet, constur adipiscing elit, sed do mod tempor incididunt ostrud </p>
                            <div class="link-effect">
                                <ul>
                                    <li>
                                        <a href="#" class="block p-img"><img src="<?php echo ASSET_URL ?>img/icons/call.png" alt=""></a>
                                        <a href="#" class="block s-img"><img src="<?php echo ASSET_URL ?>img/icons/call-hover.png" alt=""></a>
                                    </li>
                                    <li>
                                        <a href="#" class="block p-img"><img src="<?php echo ASSET_URL ?>img/icons/facebook.png" alt=""></a>
                                        <a href="#" class="block s-img"><img src="<?php echo ASSET_URL ?>img/icons/facebook-hover.png" alt=""></a>
                                    </li>
                                    <li>
                                        <a href="#" class="block p-img"><img src="<?php echo ASSET_URL ?>img/icons/twitter.png" alt=""></a>
                                        <a href="#" class="block s-img"><img src="<?php echo ASSET_URL ?>img/icons/twitter-hover.png" alt=""></a>
                                    </li>
                                    <li>
                                        <a href="#" class="block p-img"><img src="<?php echo ASSET_URL ?>img/icons/google-plus.png" alt=""></a>
                                        <a href="#" class="block s-img"><img src="<?php echo ASSET_URL ?>img/icons/google-plus-hover.png" alt=""></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-4 col-md-6 col-12">
                    <div class="single-team-member mt-md-30 mt-sm-30">
                        <div class="member-image">
                            <a href="#" class="block">
                                <img src="<?php echo ASSET_URL ?>img/team/4.png" alt="">
                            </a>
                        </div>
                        <div class="member-text">
                            <h3><a href="#">Rose Smith</a></h3>
                            <span>Financial Consultant</span>
                            <p>Astar Investment Limited amet sit amet, constur adipiscing elit, sed do mod tempor incididunt ostrud </p>
                            <div class="link-effect">
                                <ul>
                                    <li>
                                        <a href="#" class="block p-img"><img src="<?php echo ASSET_URL ?>img/icons/call.png" alt=""></a>
                                        <a href="#" class="block s-img"><img src="<?php echo ASSET_URL ?>img/icons/call-hover.png" alt=""></a>
                                    </li>
                                    <li>
                                        <a href="#" class="block p-img"><img src="<?php echo ASSET_URL ?>img/icons/facebook.png" alt=""></a>
                                        <a href="#" class="block s-img"><img src="<?php echo ASSET_URL ?>img/icons/facebook-hover.png" alt=""></a>
                                    </li>
                                    <li>
                                        <a href="#" class="block p-img"><img src="<?php echo ASSET_URL ?>img/icons/twitter.png" alt=""></a>
                                        <a href="#" class="block s-img"><img src="<?php echo ASSET_URL ?>img/icons/twitter-hover.png" alt=""></a>
                                    </li>
                                    <li>
                                        <a href="#" class="block p-img"><img src="<?php echo ASSET_URL ?>img/icons/google-plus.png" alt=""></a>
                                        <a href="#" class="block s-img"><img src="<?php echo ASSET_URL ?>img/icons/google-plus-hover.png" alt=""></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <!-- Team Area End -->
    <!-- testimonial Carousel Start -->
    <!-- <div class="testmonial-carousel bg-light ptb-70">
        <div class="container">
            <div class="row">
                <div class="col-xl-8 offset-xl-2 col-lg-10 offset-lg-1">
                    <div class="testmonial-wrapper text-center">
                        <div class="single-testi">
                            <div class="testi-img">
                                <img src="<?php echo ASSET_URL ?>img/testi/1.jpg" alt="">
                            </div>
                            <div class="testi-text">
                                <p>Astar Investment Limited amet sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequatuis aute irure</p>
                                <h5><a href="#">Andrew Williams</a></h5>
                                <span>CEO &amp; Founder</span>
                            </div>
                        </div>
                        <div class="single-testi">
                            <div class="testi-img">
                                <img src="<?php echo ASSET_URL ?>img/testi/1.jpg" alt="">
                            </div>
                            <div class="testi-text">
                                <p>Astar Investment Limited amet sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequatuis aute irure</p>
                                <h5><a href="#">Andrew Williams</a></h5>
                                <span>CEO &amp; Founder</span>
                            </div>
                        </div>
                        <div class="single-testi">
                            <div class="testi-img">
                                <img src="<?php echo ASSET_URL ?>img/testi/1.jpg" alt="">
                            </div>
                            <div class="testi-text">
                                <p>Astar Investment Limited amet sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequatuis aute irure</p>
                                <h5><a href="#">Andrew Williams</a></h5>
                                <span>CEO &amp; Founder</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <!-- testimonial Carousel End -->
    <!-- Blog Area Start -->
    <!-- <div class="blog-area  pb-115">
        <div class="container">
            <div class="row">
                <div class="col-xl-8 offset-xl-2 col-lg-10 offset-lg-1">
                    <div class="section-title text-center mb-55">
                        <h2>From Blog</h2>
                        <p>Astar Investment Limited amet sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco </p>
                    </div>
                </div>
            </div>
            <div class="blog-wrapper">
                <div class="col-12">
                    <div class="single-blog">
                        <div class="blog-image">
                            <a href="blog-details.html"><img src="<?php echo ASSET_URL ?>img/blog/1.jpg" alt=""></a>
                            <span>10 January, 2022</span>
                        </div>
                        <div class="blog-text">
                            <h4><a href="blog-details.html">Financial Meeting</a></h4>
                            <p>Astar Investment Limited consectetur adipiscing elited do eiusmod tempor incididnt mint </p>
                            <a href="blog-details.html">Read More</a>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="single-blog">
                        <div class="blog-image">
                            <a href="blog-details.html"><img src="<?php echo ASSET_URL ?>img/blog/2.jpg" alt=""></a>
                            <span>08 January, 2022</span>
                        </div>
                        <div class="blog-text">
                            <h4><a href="blog-details.html">Financial Planning</a></h4>
                            <p>Astar Investment Limited consectetur adipiscing elited do eiusmod tempor incididnt mint </p>
                            <a href="blog-details.html">Read More</a>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="single-blog">
                        <div class="blog-image">
                            <a href="blog-details.html"><img src="<?php echo ASSET_URL ?>img/blog/3.jpg" alt=""></a>
                            <span>06 January, 2022</span>
                        </div>
                        <div class="blog-text">
                            <h4><a href="blog-details.html">Financial Tips and Tricks</a></h4>
                            <p>Astar Investment Limited consectetur adipiscing elited do eiusmod tempor incididnt mint </p>
                            <a href="blog-details.html">Read More</a>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="single-blog">
                        <div class="blog-image">
                            <a href="blog-details.html"><img src="<?php echo ASSET_URL ?>img/blog/4.jpg" alt=""></a>
                            <span>04 January, 2022</span>
                        </div>
                        <div class="blog-text">
                            <h4><a href="blog-details.html">Financial Investment</a></h4>
                            <p>Astar Investment Limited consectetur adipiscing elited do eiusmod tempor incididnt mint </p>
                            <a href="blog-details.html">Read More</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <!-- Blog Area End -->
    <!--Start of Client area-->
    <div class="client-area ptb-40 bg-light">
        <div class="container">
            <div class="client-carousel">
                <div class="col-12">
                    <div class="single-client block">
                        <a href="#" class="block">
                            <span class="p-images"><img src="<?php echo ASSET_URL ?>img/client/1.png" alt=""></span>
                            <span class="s-images"><img src="<?php echo ASSET_URL ?>img/client/1-hover.png" alt=""></span>
                        </a>
                    </div>
                </div>
                <div class="col-12">
                    <div class="single-client block">
                        <a href="#" class="block">
                            <span class="p-images"><img src="<?php echo ASSET_URL ?>img/client/2.png" alt=""></span>
                            <span class="s-images"><img src="<?php echo ASSET_URL ?>img/client/2-hover.png" alt=""></span>
                        </a>
                    </div>
                </div>
                <div class="col-12">
                    <div class="single-client block">
                        <a href="#" class="block">
                            <span class="p-images"><img src="<?php echo ASSET_URL ?>img/client/3.png" alt=""></span>
                            <span class="s-images"><img src="<?php echo ASSET_URL ?>img/client/3-hover.png" alt=""></span>
                        </a>
                    </div>
                </div>
                <div class="col-12">
                    <div class="single-client block">
                        <a href="#" class="block">
                            <span class="p-images"><img src="<?php echo ASSET_URL ?>img/client/4.png" alt=""></span>
                            <span class="s-images"><img src="<?php echo ASSET_URL ?>img/client/4-hover.png" alt=""></span>
                        </a>
                    </div>
                </div>
                <div class="col-12">
                    <div class="single-client block">
                        <a href="#" class="block">
                            <span class="p-images"><img src="<?php echo ASSET_URL ?>img/client/5.png" alt=""></span>
                            <span class="s-images"><img src="<?php echo ASSET_URL ?>img/client/5-hover.png" alt=""></span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--End of Client area-->

    <?php include 'include/footer.php'; ?>

</body>

</html>