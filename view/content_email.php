<html>

<head>
    <meta content="text/html; charset=UTF-8" http-equiv="content-type">

    <style>
        .container {
            width: 100%;
            padding-right: 15px;
            padding-left: 15px;
            margin-right: auto;
            margin-left: auto;
            max-width: 720px;
        }
    </style>
</head>

<body>
    <div class="container">

        <div>
            <img src="../asset/img/email/logo.jpg" alt="Logo" style="width: 100%">
        </div>
        <h2 style="margin-bottom: 20px;margin-top:20px;text-align:center;font-family:sans-serif"><?php echo $subject; ?></h2>
        <hr />
        <h5 style=" font-family:system-ui;">From : <?php echo $name; ?></h5>
        <h5 style=" font-family:system-ui;">Email : <?php echo $from; ?></h5>
        <div style="text-align: justify; font-family:system-ui;padding:0px 20px 0px 20px">
            <?php echo $message; // Tampilkan isi pesan 
            ?>
        </div>
    </div>
</body>

</html>