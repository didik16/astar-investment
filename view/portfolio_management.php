<?php include 'config/includeWithVariables.php'; ?>


<!doctype html>
<html class="no-js" lang="en">

<?php includeWithVariables('view/include/script_header.php', array('title' => 'Portfolio Management')); ?>

<body>
    <?php include 'include/header.php'; ?>


    <!-- breadcrumbs Area Start-->
    <div class="breadcrumbs-area bg-overlay-dark bg-9" id="paralax" style="background-image:linear-gradient(rgb(255 255 255 / 47%) 68%, rgb(31 32 32) 100%), url(<?php echo ASSET_URL ?>img/banner/20.jpg)">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumbs-text text-left">
                        <h2>PORTFOLIO MANAGEMENT</h2>
                        <!-- <div class="breadcrumbs-bar">
                            <ul class="breadcrumbs">
                                <li>GET TO KNOW YOUR BROKERAGE</li>
                            </ul>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- breadcrumbs Area End -->

    <!-- <section class="section section-md bg-default novi-background"> -->
    <div class="container p-5">
        <div class="row justify-content-md-center ">

            <div class="col-md-8">
                <!-- Blurb circle-->
                <article class="blurb blurb-circle">
                    <div class="unit flex-sm-row unit-spacing-md">

                        <div class="unit__body">
                            <p>When it comes to your investments, you need a broker who can manage your portfolio and keep it up to date as your financial needs change. Here at Ford Beckett we offer our clients a fully customizable portfolio management service which includes a diverse range of fixed income, stocks, government bonds, equity and more. Being an international brokerage with a large network of financial establishments who we co-operate with we are able to provide both unique and tailored investment services to clients situated on all six of the seven continents.</p>

                            <p>When working with a Ford Beckett broker, they will get to know you and ask questions about your investing preferences, and about your personal circumstances. The reason that we take your portfolio to a more personal level is that depending on your tolerance to risks, and preference in market industries, it will open up different channels for your capital to be invested in. Not only does it stop there, the more we understand about your situation, the more tailored your portfolio will be. We believe that investors should not need to be pooled into investments or be part of mutual funds, We believe that when you invest you want to feel like you truly own what you have invested into, which is why Ford Beckett prefers to offer you a securities product that you will own solely. With decades of experience in the financial industry we believe that you as the portfolio holder should be in control of your investments at all times.</p>

                            <p>When working with Ford Beckett to create a financial portfolio we offer you the advice you need when you need it, we don’t believe in giving you investments that you don’t need, so we will never make the decision for you. We give you the information and in depth analysis that you need to make a well informed decision. Every member of the team here at Ford Beckett has extensive knowledge and insight into the markets, and will be there to assist you with every step of your portfolio from creation through to management and beyond.</p>

                            <p>No matter which decisions you make regarding your investments, and investment choices, our team of industry professionals will present you with all of the necessary information and advice, which will ensure that you are making the best decisions for your portfolio’s. Each member of the Ford Beckett team are trained experts in the creation and implementation of highly researched investment vehicles that are chosen with your unique situation in mind.</p>
                        </div>
                    </div>
                </article>
            </div>


            <?php include 'include/market-update.php'; ?>

        </div>
    </div>
    <!-- </section> -->


    <?php include 'include/footer.php'; ?>

</body>

</html>