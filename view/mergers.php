<?php include 'config/includeWithVariables.php'; ?>


<!doctype html>
<html class="no-js" lang="en">

<?php includeWithVariables('view/include/script_header.php', array('title' => 'Why Us')); ?>

<body>
    <?php include 'include/header.php'; ?>


    <!-- breadcrumbs Area Start-->
    <div class="breadcrumbs-area bg-overlay-dark bg-9" id="paralax" style="background-image:linear-gradient(rgb(255 255 255 / 47%) 68%, rgb(31 32 32) 100%), url(<?php echo ASSET_URL ?>img/banner/17.jpg)">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumbs-text text-left">
                        <h2>MERGERS & ACQUISITIONS</h2>
                        <!-- <div class="breadcrumbs-bar">
                            <ul class="breadcrumbs">
                                <li>GET TO KNOW YOUR BROKERAGE</li>
                            </ul>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- breadcrumbs Area End -->

    <!-- <section class="section section-md bg-default novi-background"> -->
    <div class="container p-5">
        <div class="row justify-content-md-center ">

            <div class="col-md-8">
                <!-- Blurb circle-->
                <article class="blurb blurb-circle">
                    <div class="unit flex-sm-row unit-spacing-md">

                        <div class="unit__body">
                            <p>Ford Beckett Group offers access to a wide range of opportunities in the mergers and acquisitions sector. By becoming one of our clients, you will have access to many global M&amp;A opportunities, which are specially chosen by our Mergers and Acquisitions department. Our team designs, creates and manages innovative and tailor made strategies for each of our clients, to enhance their portfolios, and help them achieve their financial goals. Ford Beckett Group have always shown excellence in both domestic and international transactions including a number of joint ventures, divestitures, mergers, acquisitions as well as corporate restructuring and more.</p>

                            <p>Being in the financial industry for as long as we have, our expert team of Merger and Acquisition specialists are able to apply their extensive knowledge and experience to global industries, and offer our clients a wider range of M&amp;A expertise, in comparison to your local brokerage. The benefits of working with an international brokerage like Ford Beckett Group is you gain access to a diverse range of services and products that will truly enhance your financial position, and add great value to your portfolio. Ford Beckett Group are proud to be able to meet and exceed our client’s expectations, and will strive to meet your short, mid and long-term goals.</p>
                        </div>
                    </div>
                </article>
            </div>


            <?php include 'include/market-update.php'; ?>

        </div>
    </div>
    <!-- </section> -->


    <?php include 'include/footer.php'; ?>

</body>

</html>