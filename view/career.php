<?php include 'config/includeWithVariables.php'; ?>


<!doctype html>
<html class="no-js" lang="en">

<?php includeWithVariables('view/include/script_header.php', array('title' => 'Careers')); ?>

<body>
    <?php include 'include/header.php'; ?>


    <!-- breadcrumbs Area Start-->
    <div class="breadcrumbs-area bg-overlay-dark bg-9" id="paralax" style="background-image:linear-gradient(rgb(255 255 255 / 47%) 68%, rgb(31 32 32) 100%), url(<?php echo ASSET_URL ?>img/banner/6.jpg)">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumbs-text text-left">
                        <h2>OUR PEOPLE ARE OUR POWER</h2>
                        <h2></h2>
                        <div class="breadcrumbs-bar">
                            <ul class="breadcrumbs">
                                <!-- <li><a href="index.html">HOME</a></li> -->
                                <li>Astar Investment Limited BELIEVE THAT WITH THE RIGHT PEOPLE WE CAN CHANGE THE WORLD OF INVESTMENTS</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- breadcrumbs Area End -->

    <!-- <section class="section section-md bg-default novi-background"> -->
    <div class="container p-5">
        <div class="row justify-content-md-center ">

            <div class="col-md-8">
                <!-- Blurb circle-->
                <article class="blurb blurb-circle">
                    <div class="unit flex-sm-row unit-spacing-md">

                        <div class="unit__body">
                            <p>Astar Investment Limited as an employer are there to ensure that all of our valued team members are challenged to be at their best performance in the workplace. This we believe truly allows each member of the team to show how valuable they are as a part of the company. By bringing out the best of all of our employees we are able to offer our clients a seamless and efficient service time and time again. </p>

                            <p>By choosing to work with us here at Astar Investment Limited, you will work alongside some of Asia’s biggest and brightest financial professionals, and gain a deep insight into how the world of investments works, while gaining the special skills we teach to ensure your clients are happy with each decision they make. </p>

                            <h4></h4>

                            <p>If you believe you have what it takes to to show us and our clients how passionate you are about their future success, please contact us today with your C.V and covering letter, and one of our team will be in touch with you. </p>

                            <h4></h4>

                            <p>(All applicants must hold a relevant degree in the financial sector, business management or similar.)</p>
                        </div>
                    </div>
                </article>
            </div>


            <?php include 'include/market-update.php'; ?>

        </div>
    </div>
    <!-- </section> -->


    <?php include 'include/footer.php'; ?>

</body>

</html>