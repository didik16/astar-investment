<?php include 'config/includeWithVariables.php'; ?>


<!doctype html>
<html class="no-js" lang="en">

<?php includeWithVariables('view/include/script_header.php', array('title' => 'Wealth Management')); ?>

<body>
    <?php include 'include/header.php'; ?>


    <!-- breadcrumbs Area Start-->
    <div class="breadcrumbs-area bg-overlay-dark bg-9" id="paralax" style="background-image:linear-gradient(rgb(255 255 255 / 47%) 68%, rgb(31 32 32) 100%), url(<?php echo ASSET_URL ?>img/banner/16.jpg)">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumbs-text text-left">
                        <h2>WEALTH MANAGEMENT</h2>
                        <h2></h2>
                        <div class="breadcrumbs-bar">
                            <ul class="breadcrumbs">
                                <!-- <li><a href="index.html">HOME</a></li> -->
                                <!-- <li>Astar Investment Limited ARE PROUD TO CATER TO A DIVERSE CLIENT BASE, AND THEIR RESEARCH TEAM ALLOWS THEM TO FOCUS ON GIVING EACH CLIENT THE INFORMATION AND OPPORTUNITIES TO MATCH THEIR NEEDS.</li> -->
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- breadcrumbs Area End -->

    <!-- <section class="section section-md bg-default novi-background"> -->
    <div class="container p-5">
        <div class="row justify-content-md-center ">

            <div class="col-md-8">
                <!-- Blurb circle-->
                <article class="blurb blurb-circle">
                    <div class="unit flex-sm-row unit-spacing-md">

                        <div class="unit__body">
                            <p>Working with Astar Investment Limited you will receive a full service wealth management and portfolio creation service. Working with one of our wealth management team, they will get to know you, both financially and circumstantially to create a portfolio that will enable you to hit the targets that you want to meet. During your close partnership with your portfolio manager they will assist you to reach where you want to be.</p>

                            <p>When we say a full service wealth management service, we see this as a professional working partnership between yourself and your broker, in which your broker will get to understand your financial requirements, and create a solution that both fits with your current financial plan, and will also benefit your portfolio in the long term. Our advisors will assist you with the tough financial decisions on investing with both the local and international markets. To give you the results you deserve, we monitor and adjust your portfolio regularly, which ensures that it is working to its full potential.</p>
                            <h4>IPO's and Capital Raisings</h4>
                            <p>Working with Astar Investment Limited, you will have access to many different investment opportunities. Being an international brokerage we have the ability to bring our clients the opportunities to be part of many blue chip Initial Public Offerings*. Depending on your risk tolerance and experience in the markets, you may also be eligible for other equity capital raising** ventures also.</p>

                            <p>*An Initial Public Offering (IPO), is the first sale of stock by a company to the public. A company can raise money by issuing either debt or equity in the form of shares. If the company has never issued equity to the public, its known as an IPO. There are many reasons why a company may decide to go public but one of the main opportunities is the access to additional capital for use in the business.</p>

                            <p>**Other Capital Raisings may involve the issue and sale of securities to a small number of investors instead of the general public. Unlike with a public offering, a formal prospectus does not have be provided for a private placement and thus it is often restricted to experienced or sophisticated investors that have a strong understanding of the process and risks involved.</p>
                        </div>
                    </div>
                </article>
            </div>


            <?php include 'include/market-update.php'; ?>

        </div>
    </div>
    <!-- </section> -->


    <?php include 'include/footer.php'; ?>

</body>

</html>