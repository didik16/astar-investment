<?php include 'config/includeWithVariables.php'; ?>


<!doctype html>
<html class="no-js" lang="en">

<?php includeWithVariables('view/include/script_header.php', array('title' => 'Institutional Trading')); ?>

<body>
    <?php include 'include/header.php'; ?>


    <!-- breadcrumbs Area Start-->
    <div class="breadcrumbs-area bg-overlay-dark bg-9" id="paralax" style="background-image:linear-gradient(rgb(255 255 255 / 47%) 68%, rgb(31 32 32) 100%), url(<?php echo ASSET_URL ?>img/banner/18.jpg)">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumbs-text text-left">
                        <h2>INSTITUTIONAL TRADING</h2>
                        <!-- <div class="breadcrumbs-bar">
                            <ul class="breadcrumbs">
                                <li>GET TO KNOW YOUR BROKERAGE</li>
                            </ul>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- breadcrumbs Area End -->

    <!-- <section class="section section-md bg-default novi-background"> -->
    <div class="container p-5">
        <div class="row justify-content-md-center ">

            <div class="col-md-8">
                <!-- Blurb circle-->
                <article class="blurb blurb-circle">
                    <div class="unit flex-sm-row unit-spacing-md">

                        <div class="unit__body">
                            <p>Ford Beckett has spent the best part of the last two decades creating an institutional trading team that companies around the world can rely on to deal with their finances. Our main goal and focus is to create and maintain a diverse blend of both domestic and international investment opportunities that will be both safe and profitable for each of our clients. When it comes to the markets, our institutional traders have experience that spans decades, which has enabled them to give a great insight into the management of your portfolios.</p>

                            <p>When it comes to trading, there are two very important factors to think about, the first being information and the second being the execution of that information in a timely manner. The information we base our strategies and investment opportunities on is both heavily researched and analyzed by some of the top names in the financial industry, thus creating a safer environment to base our portfolios on. With the information we gather, combined with the vast years of experience our portfolio managers have we are able to execute both buying and selling of holdings at the right time and the right price, enabling your portfolio to yield greater profits. Combining both these factors with the advanced technology we use here at Ford Beckett, our team of institutional traders have fast become one of the leading investment teams in Asia.</p>

                            <p>In our almost twenty years of operation Ford Beckett knows that no investment can be successful without in depth research and analysis of the opportunity, Even the greatest sounding ideas and plans would be rendered completely pointless without being backed by the correct information. To maximize your R.O.I and to make your portfolios execution as efficient as possible, our team of Institutional traders work closely with both our research department and market analysts to bring you the right information at the right time.</p>

                            <p>The reason Ford Beckett has been so successful, is that we assist our international and local client base with the most accurate and timely research that is backed by our research. We are not focused on generating commissions, we are determined to put our clients financial futures first, which is why when it comes to the information we give you, we make sure it has the Ford Beckett seal of approval. We know that by giving our clients portfolios the care and attention they deserve, it will show our clients that we are truly here for you. Not only do we provide each of our clients with the information they need, but we also give them every possible tool they require to keep up to date with the ever evolving markets. We are proud to give our clients the advice and information they deserve year after year.</p>

                        </div>
                    </div>
                </article>
            </div>


            <?php include 'include/market-update.php'; ?>

        </div>
    </div>
    <!-- </section> -->


    <?php include 'include/footer.php'; ?>

</body>

</html>