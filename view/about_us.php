<?php include 'config/includeWithVariables.php'; ?>


<!doctype html>
<html class="no-js" lang="en">

<?php includeWithVariables('view/include/script_header.php', array('title' => 'About Us')); ?>

<body>
    <?php include 'include/header.php'; ?>


    <!-- breadcrumbs Area Start-->
    <div class="breadcrumbs-area bg-overlay-dark bg-9" id="paralax" style="background-image:linear-gradient(rgb(255 255 255 / 47%) 68%, rgb(31 32 32) 100%), url(<?php echo ASSET_URL ?>img/banner/4.jpg)">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumbs-text text-left">
                        <h2>ABOUT US</h2>
                        <h2></h2>
                        <div class="breadcrumbs-bar">
                            <ul class="breadcrumbs">
                                <!-- <li><a href="index.html">HOME</a></li> -->
                                <li>GET TO KNOW YOUR BROKERAGE</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- breadcrumbs Area End -->

    <!-- <section class="section section-md bg-default novi-background"> -->
    <div class="container p-5">
        <div class="row justify-content-md-center ">

            <div class="col-md-8">
                <!-- Blurb circle-->
                <article class="blurb blurb-circle">
                    <div class="unit flex-sm-row unit-spacing-md">

                        <div class="unit__body">
                            <p>Astar Investment Limited, over the years have built a strong reputation within the financial industry, by bringing the best out of their team of financial experts who share years of experience in the industry, and combining their extensive knowledge with the advanced technology to ensure we generate our clients returns that meet and exceed their goals. Every Astar Investment Limited financial advisor is trained to provide our vast client base with a large number of services and strategies that can replace or co-exist with their current financial plans. Building a fully unique portfolio built around a clients needs and goals is not a straightforward task, when you have little experience with the markets, which is why when working with our clients we present them all the necessary information and figures that they need to make the right decisions in the international markets.</p>

                            <p>Over our years of operation we have shared in many successful opportunities, and have produced exceptional results for our clients utilizing all of the minds of brokers and researchers that we have with us here. The advice which we offer to our clients has been extensively researched and analyzed by our specialist teams, that allows us as a company to show our clients the growth they need in an investment opportunity. Astar Investment Limited knows that when our clients choose to make investments, they need to understand where there capital is working and how it will achieve what is needed of it.</p>

                            <p>By choosing a Astar Investment Limited advisor, your portfolio will gain access to years of industry knowledge, and access to one of the largest financial networks of in depth market analysis, economist predictions, and research teams in the world. We aim to give our clients a deeper insight into the financial industry and investment marketplace to ensure their comfort while entering the markets with their capital. By working with Astar Investment Limited and utilizing our wide range of services, you will be able to speak with some of Asia’s most experienced and educated financial professionals, who are able to give to you the most accurate and up to date information regarding your holdings, as well as analysis to ensure that you feel you have made the right decision with your portfolio.</p>

                            <p>Here at Astar Investment Limited, we believe that every person should have an equal opportunity to invest in the markets and see the returns that they deserve, which is why we do not judge a client based on their initial investment, it is with great pleasure that we would be given the opportunity to turn your capital no matter how big or small into an investment portfolio you can rely on, and be proud of. Whether you are a small, mid or large capital investor, we will work with you to turn your financial goals and aspirations into a reality.</p>
                        </div>
                    </div>
                </article>
            </div>


            <?php include 'include/market-update.php'; ?>

        </div>
    </div>
    <!-- </section> -->


    <?php include 'include/footer.php'; ?>

</body>

</html>