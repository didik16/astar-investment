<?php include 'config/includeWithVariables.php'; ?>


<!doctype html>
<html class="no-js" lang="en">

<?php includeWithVariables('view/include/script_header.php', array('title' => 'ETF Trading')); ?>

<body>
    <?php include 'include/header.php'; ?>


    <!-- breadcrumbs Area Start-->
    <div class="breadcrumbs-area bg-overlay-dark bg-9" id="paralax" style="background-image:linear-gradient(rgb(255 255 255 / 47%) 68%, rgb(31 32 32) 100%), url(<?php echo ASSET_URL ?>img/banner/13.jpg)">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumbs-text text-left">
                        <h2>EXCHANGE TRADED FUNDS (ETFS)
                        </h2>
                        <div class="breadcrumbs-bar">
                            <ul class="breadcrumbs">
                                <!-- <li>Astar Investment Limited BELIEVE THAT WITH THE RIGHT PEOPLE WE CAN CHANGE THE WORLD OF INVESTMENTS</li> -->
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- breadcrumbs Area End -->

    <!-- <section class="section section-md bg-default novi-background"> -->
    <div class="container p-5">
        <div class="row justify-content-md-center ">

            <div class="col-md-8">
                <!-- Blurb circle-->
                <article class="blurb blurb-circle">
                    <div class="unit flex-sm-row unit-spacing-md">

                        <div class="unit__body">
                            <p>Exchange traded funds or ETF’s are index funds that are list and trade on exchanges in the same way as stocks trade. An ETF can be a combination of bonds, stocks, and commodities that are trading on the same index. The daily trading value of any given ETF is determined by calculating the net asset value of all stocks, commodities and bonds that the Exchange traded fund may comprise of. Over the last 5 years Exchange traded funds have gained a lot of positive movement from investors around the world, and have been implemented in many novice, and experienced investor portfolios. This mainly driven by investors needs to have a lower risk addition to their portfolios. </p>

                            <p>The majority of ETF’s provide the investor with returns that would be similar to the total returns of securities covered in the chosen index. Investing into Exchange traded funds is a very good way of enhancing your portfolio, by allowing it more exposure over a broader range of investment vehicles, and can be a very useful tool to diversifying your holdings.</p>

                            <p>Some of the major benefits to trading ETF investments are that ETF investments broaden your portfolio and diversify your holdings, while keeping liquidity to a maximum. In the same way a stock can be traded, an ETF acts in the exact same way, meaning you are free to buy and sell at any time.</p>
                        </div>
                    </div>
                </article>
            </div>


            <?php include 'include/market-update.php'; ?>

        </div>
    </div>
    <!-- </section> -->


    <?php include 'include/footer.php'; ?>

</body>

</html>