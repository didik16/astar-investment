<?php include 'config/includeWithVariables.php'; ?>


<!doctype html>
<html class="no-js" lang="en">

<?php includeWithVariables('view/include/script_header.php', array('title' => 'Why Us')); ?>

<body>
    <?php include 'include/header.php'; ?>


    <!-- breadcrumbs Area Start-->
    <div class="breadcrumbs-area bg-overlay-dark bg-9" id="paralax" style="background-image:linear-gradient(rgb(255 255 255 / 47%) 68%, rgb(31 32 32) 100%), url(<?php echo ASSET_URL ?>img/banner/5.jpg)">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumbs-text text-left">
                        <h2>CHOOSING THE RIGHT BROKER</h2>
                        <!-- <div class="breadcrumbs-bar">
                            <ul class="breadcrumbs">
                                <li>GET TO KNOW YOUR BROKERAGE</li>
                            </ul>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- breadcrumbs Area End -->

    <!-- <section class="section section-md bg-default novi-background"> -->
    <div class="container p-5">
        <div class="row justify-content-md-center ">

            <div class="col-md-8">
                <!-- Blurb circle-->
                <article class="blurb blurb-circle">
                    <div class="unit flex-sm-row unit-spacing-md">

                        <div class="unit__body">
                            <h4 class="mb-3">Why Choose Us to be Your Financial Advisors?</h4>

                            <p>Astar Investment Limited offers a wide range of investment solutions and advisory services to our clients. Not only offering standard brokerage packages, Astar Investment Limited also offers uniquely designed portfolios for individuals and corporations who are in search of a risk adverse strategy to protect their investment capital as much as they can. Although we are a medium sized brokerage firm, our team here at Astar Investment Limited shares centuries of experience in the financial markets, and the financial industry as a whole.</p>


                            <h5></h5>
                            <p>As a company we offer our clients services ranging from investment banking, private wealth management, asset management, stockbroking and more. Every Member of the Astar Investment Limited team are experienced and highly educated in their chosen field, so when you decide to work with one of our brokers, and make the most of their background, you are in very safe hands.Being a brokerage that works across the international marketplace, we have access to a large number of opportunities that your local brokerage, and local brokers do not have, and we have the facilities and team of researchers and analysts to make the most of those opportunities.</p>

                            <h5></h5>
                            <p>Working with Astar Investment Limited you will have access to our international network of financial experts and trading resources. With the ability to draw from more than just our own knowledge our advisors have the capability to offer you a comprehensive range of investments, that have faced in depth research to ensure that you receive the advice you need, when you need it. Our team of industry experts are comprised of financial professionals who have backgrounds in stockbroking, financial planning, market analysis and corporate advisory. Our advisors work together to bring you the direct access to the right information insights and investment opportunities that you deserve, this in turn provides us with the right foundations for the longevity of your account(s) with us.</p>


                            <h5></h5>
                            <p>When working with Astar Investment Limited you will experience a personal convenient service with no long phone holds or voice mails. We pride ourselves on our client communication, which is why we make sure we are available at your convenience either by Email, Telephone, or even welcome you to visit one of our offices. We do this to enable a seamless communication between broker and client. To find out more about the services we provide, and how we aim to bring the best to our clients, please do not hesitate to <a href="contact.php" title="Connect with Us - Astar Investment Limited">contact us</a>.</p>


                        </div>
                    </div>
                </article>
            </div>


            <?php include 'include/market-update.php'; ?>

        </div>
    </div>
    <!-- </section> -->


    <?php include 'include/footer.php'; ?>

</body>

</html>