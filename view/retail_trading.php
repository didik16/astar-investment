<?php include 'config/includeWithVariables.php'; ?>


<!doctype html>
<html class="no-js" lang="en">

<?php includeWithVariables('view/include/script_header.php', array('title' => 'Retail Trading')); ?>

<body>
    <?php include 'include/header.php'; ?>


    <!-- breadcrumbs Area Start-->
    <div class="breadcrumbs-area bg-overlay-dark bg-9" id="paralax" style="background-image:linear-gradient(rgb(255 255 255 / 47%) 68%, rgb(31 32 32) 100%), url(<?php echo ASSET_URL ?>img/banner/19.jpg)">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumbs-text text-left">
                        <h2>RETAIL TRADING</h2>
                        <!-- <div class="breadcrumbs-bar">
                            <ul class="breadcrumbs">
                                <li>GET TO KNOW YOUR BROKERAGE</li>
                            </ul>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- breadcrumbs Area End -->

    <!-- <section class="section section-md bg-default novi-background"> -->
    <div class="container p-5">
        <div class="row justify-content-md-center ">

            <div class="col-md-8">
                <!-- Blurb circle-->
                <article class="blurb blurb-circle">
                    <div class="unit flex-sm-row unit-spacing-md">

                        <div class="unit__body">
                            <p>When it comes to investing your capital into the markets, Astar Investment Limited knows that you need a broker who understands you and your needs. Each of our clients receive the same level of professional service and care no matter how they choose to invest. Our main focus here at Astar Investment Limited is to offer our clients in depth analysis of companies, their products and the services they provide. The reason our clients choose to use our services, is combining our wealth of experience with the technology we use, helps us to stand out in the crowd and show our clients the returns they are truly looking for. The reason we put so much care and attention into each clients portfolio is so that they can achieve their goals and then reach further to make new ones.</p>

                            <p>The difference between retail trading and institutional trading, is that although both comprised of a selection of investment vehicles, retail trading portfolios consist mainly of small-mid cap companies, this due to the fact it allows your broker to work on the lower share price points. This in turn allows your portfolio to be diversified across many different companies in a number of different sectors, and although it enables more exposure on your portfolio the opportunity is there to be vastly more profitable. Astar Investment Limited believe that when investing your portfolio should show quality over quantity, and our brokers believe this too, which is why no matter what your initial investment capital is, you will receive the same great level of service and expertise as our larger clients.</p>

                            <p>All of our investment vehicles are focused on giving each individual client the right opportunities that will benefit them, as well as offering in depth financial advice and research into those opportunities. With the decades of experience our retail trading team share, combined with the backing of one of Asia’s most efficient research teams, we are proud to offer our clients the best possible service in the markets. Astar Investment Limited are focused on assisting you in reaching your financial goal, whether it’s aimed towards creating an education fund, estate plan, or retirement plan, our brokers will work closely with you to achieve your financial goals in a time that is suitable for you. We believe there is no set plan to investing, which is why we take a very personal approach to your portfolio, that way we can offer you what you are comfortable with, and what you need.</p>
                        </div>
                    </div>
                </article>
            </div>


            <?php include 'include/market-update.php'; ?>

        </div>
    </div>
    <!-- </section> -->


    <?php include 'include/footer.php'; ?>

</body>

</html>