 <!--Start of Footer Widget-area-->
 <div class="footer-widget-area bg-4 overlay-blue pt-110 pb-100">
     <div class="container">
         <div class="row">
             <div class="col-xl-3 col-lg-3 col-md-6 col-12 order-md-1 order-1">
                 <div class="single-footer-widget">
                     <div class="footer-logo">
                         <a href="index.html"><img src="<?php echo ASSET_URL ?>img/logo/footer-logo.png" alt="FINANCO" width="100%"></a>
                     </div>
                     <p>Here at Astar Investment Limited, we have a team of highly experienced investment managers that are well versed in an large number of wealth management services.</p>
                     <form action="#" id="mc-form" class="mc-form fix">
                         <div class="subscribe-form">
                             <input id="mc-email" type="email" name="email" placeholder="Email for Newsletter">
                             <button id="mc-submit" type="submit"><i class="fa fa-send"></i></button>
                         </div>
                     </form>
                     <!-- mailchimp-alerts Start -->
                     <div class="mailchimp-alerts text-centre fix text-small">
                         <div class="mailchimp-submitting"></div><!-- mailchimp-submitting end -->
                         <div class="mailchimp-success"></div><!-- mailchimp-success end -->
                         <div class="mailchimp-error"></div><!-- mailchimp-error end -->
                     </div>
                     <!-- mailchimp-alerts end -->
                 </div>
             </div>
             <div class="col-xl-3 col-lg-3 col-md-6 col-12 order-md-3 order-2">
                 <div class="single-footer-widget mt-sm-30">
                     <h3>POPULAR POST</h3>
                     <ul class="footer-list">
                         <li><a href="<?php echo BASE_URL ?>investing_and_trading" title="Investing and Trading - Astar Investment Limited">Investing and Trading</a></li>
                         <li><a href="<?php echo BASE_URL ?>options_trading" title="Options Trading - Astar Investment Limited">Options Trading</a></li>
                         <li><a href="<?php echo BASE_URL ?>etf_trading" title="ETF Trading - Astar Investment Limited">ETF Trading</a></li>
                         <li><a href="<?php echo BASE_URL ?>investment_banking" title="Investment Banking">Investment Banking</a></li>
                         <li><a href="<?php echo BASE_URL ?>fixed_income_trading" title="Fixed Income Trading">Fixed Income Trading</a></li>
                         <li><a href="<?php echo BASE_URL ?>wealth_management" title="Wealth Management - Astar Investment Limited">Wealth Management</a></li>
                         <li><a href="<?php echo BASE_URL ?>mergers" title="Mergers &amp; Acquisitions - Astar Investment Limited">Mergers</a></li>
                         <li><a href="<?php echo BASE_URL ?>institutional_trading" title="Institutional Trading - Astar Investment Limited">Institutional Trading</a></li>
                         <li><a href="<?php echo BASE_URL ?>retail_trading" title="Retail Trading - Astar Investment Limited">Retail Trading</a></li>
                         <li><a href="<?php echo BASE_URL ?>portfolio_management" title="Portfolio Management - Astar Investment Limited">Portfolio Management</a></li>
                         <li><a href="<?php echo BASE_URL ?>ecm_trading" title="ECM Trading - Astar Investment Limited">ECM Trading</a></li>
                     </ul>
                 </div>
             </div>
             <div class="col-xl-3 col-lg-3 col-md-6 col-12 order-md-2 order-3">
                 <div class="single-footer-widget">
                     <h3>QUICK LINK</h3>
                     <ul class="footer-list">
                         <li><a href="<?php echo BASE_URL ?>about_us">About us</a></li>
                         <li><a href="<?php echo BASE_URL ?>">Fees and Pricing</a></li>
                         <li><a href="<?php echo BASE_URL ?>careers">Careers</a></li>
                         <li><a href="<?php echo BASE_URL ?>retirement_calculator">Retirement Planning</a></li>
                         <li><a href="<?php echo BASE_URL ?>why_us">Why Us</a></li>
                         <li><a href="<?php echo BASE_URL ?>our_team">Our Team</a></li>
                     </ul>
                 </div>
             </div>

             <div class="col-xl-3 col-lg-3 col-md-6 col-12 order-md-4 order-4">
                 <div class="single-footer-widget mt-sm-30">
                     <h3>CONTACT US</h3>
                     <div class="footer-contact-info">
                         <img src="<?php echo ASSET_URL ?>img/icons/f-map.png" alt="">
                         <span class="block">One Hennessy, Level 12, 1 Hennessy Road, Wan Chai, Hong Kong, China S.A.R</span>
                     </div>
                     <div class="footer-contact-info">
                         <img src="<?php echo ASSET_URL ?>img/icons/f-phone.png" alt="">
                         <span class="block">Telephone : +852 3018 3094<br>
                             Telephone : +852 3016 2332</span>
                     </div>
                     <div class="footer-contact-info">
                         <img src="<?php echo ASSET_URL ?>img/icons/f-globe.png" alt="">
                         <span class="block">
                             <a href="mailto:info@fordbeckett.com" style="color: #cccccc!important;">Email : info@fordbeckett.com
                             </a>
                         </span>
                     </div>
                 </div>
             </div>
         </div>
     </div>
 </div>
 <!--End of Footer Widget-area-->
 <!-- Start of Footer area -->
 <footer class="footer-area bg-blue text-center ptb-30">
     <div class="container">
         <div class="row">
             <div class="col-md-12">
                 <div class="footer-text">
                     <span class="block">Copyright&copy; <?php echo date('Y') ?> <a href="#">Astar Investment Limited</a>. All rights reserved.</span>
                 </div>
             </div>
         </div>
     </div>
 </footer>
 <!-- End of Footer area -->

 <!-- All js here -->
 <script src="<?php echo ASSET_URL ?>js/vendor/modernizr-3.6.0.min.js"></script>
 <script src="<?php echo ASSET_URL ?>js/vendor/jquery-3.6.0.min.js"></script>
 <script src="<?php echo ASSET_URL ?>js/vendor/jquery-migrate-3.3.2.min.js"></script>
 <script src="<?php echo ASSET_URL ?>js/popper.min.js"></script>
 <script src="<?php echo ASSET_URL ?>js/bootstrap.min.js"></script>
 <script src="<?php echo ASSET_URL ?>js/owl.carousel.min.js"></script>
 <!-- <script src="<?php echo ASSET_URL ?>js/ajax-mail.js"></script> -->
 <script src="<?php echo ASSET_URL ?>js/jquery.ajaxchimp.min.js"></script>
 <script src="<?php echo ASSET_URL ?>js/jquery.magnific-popup.js"></script>
 <script src="<?php echo ASSET_URL ?>js/counterup.js"></script>
 <script src="<?php echo ASSET_URL ?>js/plugins.js"></script>
 <script src="<?php echo ASSET_URL ?>js/canvasjs.min.js"></script>
 <script src="<?php echo ASSET_URL ?>js/canvas-active-2.js"></script>
 <script src="<?php echo ASSET_URL ?>js/main.js"></script>
 <a id="scrollUp" href="#top" style="position: fixed; z-index: 2147483647; display: inline;">Scroll to top</a>

 <script type="text/javascript">
     $(document).ready(function() {

         // Find the initial scroll top when the page is loaded.
         var initScrollTop = $(window).scrollTop();

         // Set the image's vertical background position based on the scroll top when the page is loaded.
         $('#paralax').css({
             'background-position': '50% ' + (initScrollTop / 4) + '%'
         });

         // When the user scrolls...
         $(window).scroll(function() {

             // Find the new scroll top.
             var scrollTop = $(window).scrollTop();

             // Set the new background position.
             $('#paralax').css({
                 'background-position': '50% ' + (scrollTop / 4) + '%'
             });

         });

     });
 </script>