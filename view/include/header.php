<?php
ob_start();
session_start();

include 'config/koneksi.php';
?>

<!-- Header Area Start -->
<header class="header-four-area">
    <!-- <div class="header-top blue-bg fix d-lg-block d-md-block d-none"> -->
    <div class="header-top blue-bg d-lg-block d-md-block d-none">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-3">
                    <!-- <span>Welcome to Astar Investment Limited</span> -->
                    <?php
                    if (isset($_SESSION['email'])) {
                    ?>
                        <div class="btn-group">
                            <button type="button" class="btn btn-danger">Halo, <?php echo $_SESSION['name'] ?></button>
                            <button type="button" class="btn btn-danger dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <div class="dropdown-menu" style="z-index: 99;">
                                <a class="dropdown-item" href="<?php echo BASE_URL ?>logout">Logout</a>
                            </div>
                        </div>
                        <!-- echo '<a href="' . BASE_URL . 'login" style="color:white!important">Halo, ' . $_SESSION['name'] . '</a>'; -->
                    <?php
                    } else {
                        echo '<a href="' . BASE_URL . 'login" style="color:white!important">Login</a>';
                    }

                    ?>

                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="header-icons">
                        <a href="#"><i class="fa fa-facebook"></i></a>
                        <a href="#"><i class="fa fa-twitter"></i></a>
                        <a href="#"><i class="fa fa-google-plus"></i></a>
                        <a href="#"><i class="fa fa-linkedin"></i></a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-3">
                    <div class="search-container">
                        <form action="#" method="post">
                            <input placeholder="" type="text">
                            <button><i class="fa fa-search"></i></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="header-main bg-white">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="header-main-content">
                        <div class="header-info">
                            <img src="<?php echo ASSET_URL ?>img/icons/phone-2.png" alt="">
                            <div class="header-info-text">
                                <h4>+852 3018 3094</h4>
                                <span>We are open 9 am - 10pm</span>
                            </div>
                        </div>
                    </div>
                    <div class="logo-wrapper">
                        <div class="logo">
                            <a href="<?php echo BASE_URL ?>"><img src="<?php echo ASSET_URL ?>/img/logo/logo.png" alt="Astar Investment Limited" width="80%" /></a>
                        </div>
                    </div>
                    <div class="quote-btn">
                        <button>Get a Quote</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Main Menu Area Start -->
    <div class="mainmenu-area fixed header-sticky d-lg-block d-md-block d-none">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-12">
                    <div class="main-menu text-center">
                        <nav>
                            <ul>
                                <li><a href="<?php echo BASE_URL ?>">HOME</a>

                                <li><a href="<?php echo BASE_URL ?>about_us">ABOUT US</a>
                                    <ul class="submenu">
                                        <li><a href="<?php echo BASE_URL ?>about_us">About Us</a></li>
                                        <li><a href="<?php echo BASE_URL ?>why_us">Why Us</a></li>
                                        <li><a href="<?php echo BASE_URL ?>careers">Careers</a></li>
                                        <li><a href="<?php echo BASE_URL ?>our_team">Our Team</a></li>
                                    </ul>
                                </li>
                                <li><a href="#">INVESTING & TRADING</a>
                                    <ul class="submenu megamenu">
                                        <li>
                                            <!-- <a href="#">Megamenu List</a> -->
                                            <ul>
                                                <li><a href="<?php echo BASE_URL ?>option_trading">Options Trading</a></li>
                                                <li><a href="<?php echo BASE_URL ?>etf_trading">ETF Trading</a></li>
                                                <li><a href="<?php echo BASE_URL ?>investment_banking">Investment Banking</a></li>
                                                <li><a href="<?php echo BASE_URL ?>fixed_income_trading">Fixed Income Trading</a></li>
                                                <li><a href="<?php echo BASE_URL ?>wealth_management">Wealth Management</a></li>
                                            </ul>
                                        </li>
                                        <li>
                                            <!-- <a href="#">Megamenu List</a> -->
                                            <ul>
                                                <li><a href="<?php echo BASE_URL ?>mergers">Mergers</a></li>
                                                <li><a href="<?php echo BASE_URL ?>institutional_trading">Institutional Trading</a></li>
                                                <li><a href="<?php echo BASE_URL ?>retail_trading">Retail Trading</a></li>
                                                <li><a href="<?php echo BASE_URL ?>portfolio_management">Portfolio Management</a></li>
                                                <li><a href="<?php echo BASE_URL ?>ecm_trading">ECM Trading</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li><a href="services.html">RETIREMENT PLANNING</a>
                                    <ul class="submenu">
                                        <li><a href="<?php echo BASE_URL ?>retirement_calculator">Retirement Planner</a></li>
                                    </ul>
                                </li>

                                <li><a href="<?php echo BASE_URL ?>research">RESEARCH</a></li>
                                <li><a href="<?php echo BASE_URL ?>contact">CONTACT</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Main Menu Area Start -->
    <!-- Mobile Menu Area start -->
    <div class="mobile-menu-area d-lg-none d-md-none d-block">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12">
                    <div class="mobile-menu">
                        <nav id="dropdown">
                            <ul>
                                <li><a href="index.html">HOME</a>
                                </li>
                                <li><a href="about.html">ABOUT US</a>
                                    <ul class="submenu">
                                        <li><a href="<?php echo BASE_URL ?>about_us">About Us</a></li>
                                        <li><a href="<?php echo BASE_URL ?>why_us">Why Us</a></li>
                                        <li><a href="<?php echo BASE_URL ?>careers">Careers</a></li>
                                        <li><a href="<?php echo BASE_URL ?>our_team">Our Team</a></li>
                                    </ul>
                                </li>
                                <li><a href="#">INVESTING & TRADING</a>
                                    <ul class="submenu megamenu">
                                        <li>
                                            <!-- <a href="#">Megamenu List</a> -->
                                            <ul>
                                                <li><a href="<?php echo BASE_URL ?>option_trading">Options Trading</a></li>
                                                <li><a href="<?php echo BASE_URL ?>etf_trading">ETF Trading</a></li>
                                                <li><a href="<?php echo BASE_URL ?>investment_banking">Investment Banking</a></li>
                                                <li><a href="<?php echo BASE_URL ?>fixed_income_trading">Fixed Income Trading</a></li>
                                                <li><a href="<?php echo BASE_URL ?>wealth_management">Wealth Management</a></li>
                                            </ul>
                                        </li>
                                        <li>
                                            <!-- <a href="#">Megamenu List</a> -->
                                            <ul>
                                                <li><a href="<?php echo BASE_URL ?>mergers">Mergers</a></li>
                                                <li><a href="<?php echo BASE_URL ?>institutional_trading">Institutional Trading</a></li>
                                                <li><a href="<?php echo BASE_URL ?>retail_trading">Retail Trading</a></li>
                                                <li><a href="<?php echo BASE_URL ?>portfolio_management">Portfolio Management</a></li>
                                                <li><a href="<?php echo BASE_URL ?>ecm_trading">ECM Trading</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li><a href="services.html">RETIREMENT PLANNING</a>
                                    <ul class="submenu">
                                        <li><a href="services-2.html">Retirement Planner</a></li>
                                    </ul>
                                </li>

                                <li><a href="<?php echo BASE_URL ?>research">RESEARCH</a></li>
                                <li><a href="<?php echo BASE_URL ?>contact">CONTACT</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Mobile Menu Area end -->
</header>
<!-- Header Area End -->