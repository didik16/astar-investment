<?php include 'config/base_url.php'; ?>

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Astar Investment Limited - <?php echo  $title ?></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Favicon Icon -->
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo ASSET_URL ?>img/favicon.ico">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,600" rel="stylesheet">

    <!-- All css here -->
    <link rel="stylesheet" href="<?php echo ASSET_URL ?>css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo ASSET_URL ?>css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo ASSET_URL ?>css/shortcode/shortcodes.css">
    <link rel="stylesheet" href="<?php echo ASSET_URL ?>css/animate.css">
    <link rel="stylesheet" href="<?php echo ASSET_URL ?>css/owl.carousel.css">
    <link rel="stylesheet" href="<?php echo ASSET_URL ?>css/style.css">
    <link rel="stylesheet" href="<?php echo ASSET_URL ?>css/responsive.css">

    <!-- <link rel="stylesheet" href="../asset/css/bootstrap.min.css">
    <link rel="stylesheet" href="../asset/css/font-awesome.min.css">
    <link rel="stylesheet" href="../asset/css/shortcode/shortcodes.css">
    <link rel="stylesheet" href="../asset/css/animate.css">
    <link rel="stylesheet" href="../asset/css/owl.carousel.css">
    <link rel="stylesheet" href="../asset/css/style.css">
    <link rel="stylesheet" href="../asset/css/responsive.css"> -->

</head>