$(document).ready(function () {

	//Sliders

	/* Current Age */
	$("[data-slider-type='currentAge']").each(function (index) {
		var initial_value = $(this).siblings("input").val();
		$(this).noUiSlider({
			start: initial_value,
			step: 1,
			range: {
				'min': [0],
				'max': [99]
			},
			format: wNumb({
				decimals: 0,
				postfix: ' years',
			})
		});
		$(this).Link('lower').to($(this).siblings("input"));
	});

	/* Retirement Years */
	$("[data-slider-type='retirementYears']").each(function (index) {
		var initial_value = $(this).siblings("input").val();
		$(this).noUiSlider({
			start: initial_value,
			step: 1,
			range: {
				'min': [2],
				'max': [99]
			},
			format: wNumb({
				decimals: 0,
				postfix: ' years',
			})
		});
		$(this).Link('lower').to($(this).siblings("input"));
	});

	/* Retirement contributionAgeSlider */
	$("[data-slider-type='contributionAgeSlider']").each(function (index) {
		var initial_value = $(this).siblings("input").val();
		$(this).noUiSlider({
			start: initial_value,
			step: 1,
			range: {
				'min': [1],
				'max': [98]
			},
			format: wNumb({
				decimals: 0,
				postfix: ' years',
			})
		});
		$(this).Link('lower').to($(this).siblings("input"));
	});

	// Rate Slider 
	$("[data-slider-type='retirementRateSliderRange']").each(function (index) {
		var initial_value = $(this).siblings("input").val();
		$(this).noUiSlider({
			start: initial_value,
			step: .001,
			range: {
				'min': [0],
				'max': [50]
			},
			format: wNumb({
				decimals: 3,
				postfix: '%',
			})
		});
		$(this).Link('lower').to($(this).siblings("input"));
	});

	//money sliders
	$("[data-slider-type='dollarAmountSlider']").each(function (index) {
		var initial_value = $(this).siblings("input").val();
		$(this).noUiSlider({
			start: initial_value,
			range: {
				'min': [0, 100],
				'75%': [100000, 1000],
				'max': [9999999]
			},
			format: wNumb({
				decimals: 0,
				thousand: ',',
				prefix: '$',
			})
		});
		$(this).Link('lower').to($(this).siblings("input"));
	});


	$("input[type='text']").on("click", function () {
		$(this).select();
	});

	$("input[type='text']").on("keyup", function () {
		refreshRetirementVariables();
		invalidRetirementValueCheck();
	});

	$("input[type='text']").on("blur", function () {
		refreshRetirementVariables();
		retirementCalculationRollup();
		retirementDomRedraw();
		invalidRetirementValueCheck();
		adjustSliders();
		refreshRetirementVariables();
		invalidRetirementValueCheck();
	});



	//variable declaration
	var minimumAnnualContribution = 0;
	var maximumAnnualContribution = 0;
	var currentPlanValue = 0;
	var beginningContributionAge = 0;
	var yearOneMinimumValue = 0;
	var yearOneMaximumValue = 0;
	var minInvestmentOverTime = 0;
	var maxInvestmentOverTime = 0;
	var minValueAtRetirement = 0;
	var maxValueAtRetirement = 0;
	var deferredMinValueAtRetirement = 0;
	var deferredMaxValueAtRetirement = 0;
	var deferredIncubationPeriod = 0;
	var retirementCurrentAgeSlider = 0;
	var retirementEnrollmentAgeSlider = 0;
	var retirementBeginningContributionAgeSlider = 0;
	var inputValue = 0;




	//Calculate button
	$("#calculateRetirement").click(function (e) {
		e.preventDefault();
		$(".ej-calculator--form").attr({
			"aria-hidden": "true",
			"role": "presentation"
		});
		refreshRetirementVariables();
		retirementCalculationRollup();
		removeIntroDOMItems();
		retirementDomRedraw();
	});

	$("#resetValues").click(function (e) {
		e.preventDefault();
		resetSliders();
		refreshRetirementVariables();
		retirementCalculationRollup();
		retirementDomRedraw();
	});

	function removeIntroDOMItems() {
		$('#calculateRetirement').hide();
		$('#resetValues').show();
		$('#postCalcMsg').show();
		$('.ej-calculator--sidebar').hide();
		$('.ej-calculator--results').show();
		$('.ej-calculator--footer').show();
	}
	function resetSliders() {
		$("#currentAgeRetirementSlider").val(0);
		$("#enrollmentRetirementAgeSlider").val(55);
		$("#beginningContributionAgeSlider").val(1);
		$("#retirementRateSlider").val(0);
		$("#minimumAnnualSlider").val(0);
		$("#maximumAnnualSlider").val(0);
		$("#currentPlanValueSlider").val(0);

	}
	function refreshRetirementVariables() {
		currentAge = parseInt($('#CurrentAge').val());
		retirementCurrentAgeSlider = parseInt($('#currentAgeRetirementSlider').val());
		intRetirementAge = parseInt($('#RetirementAge').val());
		retirementEnrollmentAgeSlider = parseInt($('#enrollmentRetirementAgeSlider').val());
		returnMultiplier = (parseFloat($('#RateOfReturn').val()) / 100) + 1;
		rateSliderValue = $('#retirementRateSlider').val();
		minimumAnnualContribution = Number($('#minAnnualContribution').val().replace(/[^0-9\.]+/g, ""));
		maximumAnnualContribution = Number($('#maxAnnualContribution').val().replace(/[^0-9\.]+/g, ""));
		minimumContribSliderValue = $('#minimumAnnualSlider').val();
		maximumContribSliderValue = $('#maximumAnnualSlider').val();
		beginningContributionAge = parseInt($('#BeginningContributionAge').val());
		retirementBeginningContributionAgeSlider = parseInt($('#beginningContributionAgeSlider').val())
		currentPlanSliderValue = $('#CurrentPlanValue').val();
		currentPlanValue = Number($('#CurrentPlanValue').val().replace(/[^0-9\.]+/g, ""));
		incubationPeriod = intRetirementAge - currentAge - 1;
		investmentYears = intRetirementAge - beginningContributionAge;
		deferredIncubationPeriod = intRetirementAge - beginningContributionAge - 1;
		yearOneMinimumValue = (currentPlanValue * returnMultiplier) + minimumAnnualContribution;
		yearOneMaximumValue = (currentPlanValue * returnMultiplier) + maximumAnnualContribution;
		minInvestmentOverTime = (minimumAnnualContribution * incubationPeriod) + currentPlanValue;
		maxInvestmentOverTime = (maximumAnnualContribution * incubationPeriod) + currentPlanValue;
	}

	function retirementCalculationRollup() {
		calculateRetirementSavings(yearOneMinimumValue, returnMultiplier, incubationPeriod, minimumAnnualContribution, 'min');
		calculateRetirementSavings(yearOneMaximumValue, returnMultiplier, incubationPeriod, maximumAnnualContribution, 'max');
		calculateRetirementSavings(yearOneMinimumValue, returnMultiplier, deferredIncubationPeriod, minimumAnnualContribution, 'defferedMin');
		calculateRetirementSavings(yearOneMaximumValue, returnMultiplier, deferredIncubationPeriod, maximumAnnualContribution, 'defferedMax');
	}

	function calculateRetirementSavings(argReturnAmount, argReturnMultiplier, argTimeInvested, argAnnualContribution, argScenario) {
		for (var i = 0, limit = argTimeInvested; i < limit; i++) {
			argReturnAmount = (argReturnAmount * argReturnMultiplier) + argAnnualContribution;
		}
		switch (argScenario) {
			case 'min':
				minValueAtRetirement = argReturnAmount;
				break;
			case 'max':
				maxValueAtRetirement = argReturnAmount;
				break;
			case 'defferedMin':
				deferredMinValueAtRetirement = argReturnAmount;
				break;
			case 'defferedMax':
				deferredMaxValueAtRetirement = argReturnAmount;
				break;
		}
	}

	function retirementDomRedraw() {
		$('#deferredMinimum').text('$' + addCommas(deferredMinValueAtRetirement.toFixed(2)));
		$('#deferredMaximum').text('$' + addCommas(deferredMaxValueAtRetirement.toFixed(2)));
		$('#minPotentialRetirementAmount').text('$' + addCommas(minValueAtRetirement.toFixed(2)));
		$('#minimumValueAtRetirement').text('$' + addCommas(minValueAtRetirement.toFixed(2)));
		$('#maxPotentialRetirementAmount').text('$' + addCommas(maxValueAtRetirement.toFixed(2)));
		$('#maximumValueAtRetirement').text('$' + addCommas(maxValueAtRetirement.toFixed(2)));
		$('#minimumScenarioHeader').html('<div><span class="helper__reader">Results for </span>Scenario 1: Your potential yield at retirement <span class="helper__reader">is </span><em>$' + addCommas(deferredMinValueAtRetirement.toFixed(2)) + '</em></div>');
		$('#minimumScenarioFooter').html('<div>If you invest <strong>$' + addCommas(minimumAnnualContribution.toFixed(2)) + '</strong> at <strong>' + addCommas($("#retirementRateSlider").val()) + '</strong> annually for <strong> ' + investmentYears + ' years</strong>. If you started saving at age ' + currentAge + ', your investment could yield <strong>$' + addCommas(minValueAtRetirement.toFixed(2)) + '</strong>.</div>');
		$('#maximumScenarioHeader').html('<div><span class="helper__reader">Results for </span>Scenario 2: Your potential yield at retirement <span class="helper__reader">is </span><em>$' + addCommas(deferredMaxValueAtRetirement.toFixed(2)) + '</em></div>');
		$('#maximumScenarioFooter').html('<div>If you invest <strong>$' + addCommas(maximumAnnualContribution.toFixed(2)) + '</strong> at <strong>' + addCommas($("#retirementRateSlider").val()) + '</strong> annually for <strong> ' + investmentYears + ' years</strong>. If you started saving at age ' + currentAge + ', your investment could yield <strong>$' + addCommas(maxValueAtRetirement.toFixed(2)) + '</strong>.</div>');
		redrawGraphs();
	}

	function redrawGraphs() {
		var scenarioOnebar_length = (deferredMinValueAtRetirement * 100) / minValueAtRetirement + "%";
		var scenarioTwoBar_length = (deferredMaxValueAtRetirement * 100) / maxValueAtRetirement + "%";
		$('#deferredMinimum').attr('style', 'width:' + scenarioOnebar_length);
		$('#deferredMaximum').attr('style', 'width:' + scenarioTwoBar_length);
	}

	function invalidRetirementValueCheck() {
		if (currentAge > beginningContributionAge && currentAge > intRetirementAge) {
			$('#CurrentAge').addClass('calculator-error');
			$('#BeginningContributionAge').addClass('calculator-error');
			$('#RetirementAge').addClass('calculator-error');
		} else if (currentAge > beginningContributionAge) {
			$('#CurrentAge').addClass('calculator-error');
			$('#BeginningContributionAge').addClass('calculator-error');
		} else if (intRetirementAge < currentAge) {
			$('#CurrentAge').addClass('calculator-error');
			$('#RetirementAge').addClass('calculator-error');
			$('#BeginningContributionAge').addClass('calculator-error');
		} else if (beginningContributionAge > intRetirementAge) {
			$('#BeginningContributionAge').addClass('calculator-error');
			$('#RetirementAge').addClass('calculator-error');
		} else {
			$('#CurrentAge').removeClass('calculator-error');
			$('#BeginningContributionAge').removeClass('calculator-error');
			$('#RetirementAge').removeClass('calculator-error');
		}

		if (minimumAnnualContribution > maximumAnnualContribution) {
			$("#minAnnualContribution").addClass('calculator-error');
			$("#maxAnnualContribution").addClass('calculator-error');
		} else {
			$("#minAnnualContribution").removeClass('calculator-error');
			$('#maxAnnualContribution').removeClass('calculator-error');
		}
	}

	function adjustSliders() {
		if (retirementCurrentAgeSlider > retirementEnrollmentAgeSlider) {
			$("#currentAgeRetirementSlider").val($('#enrollmentRetirementAgeSlider').val());
		}
		if (retirementBeginningContributionAgeSlider < retirementCurrentAgeSlider) {
			$("#beginningContributionAgeSlider").val($('#currentAgeRetirementSlider').val());
		}
		if (retirementBeginningContributionAgeSlider > retirementEnrollmentAgeSlider) {
			$("#beginningContributionAgeSlider").val($('#enrollmentRetirementAgeSlider').val());
		}
		if (maximumAnnualContribution < minimumAnnualContribution) {
			$("#maximumAnnualSlider").val($('#minimumAnnualSlider').val());
		}
	}

	function addCommas(nStr) {
		nStr += '';
		x = nStr.split('.');
		x1 = x[0];
		x2 = x.length > 1 ? '.' + x[1] : '';
		var rgx = /(\d+)(\d{3})/;
		while (rgx.test(x1)) {
			x1 = x1.replace(rgx, '$1' + ',' + '$2');
		}
		return x1 + x2;
	}


	//slider event handlers
	$(".ej-ui-nouislider").on({
		slide: function () {
			refreshRetirementVariables();
			retirementCalculationRollup();
			invalidRetirementValueCheck();
			retirementDomRedraw();
		},
		set: function () {
			refreshRetirementVariables();
			retirementCalculationRollup();
			invalidRetirementValueCheck();
			retirementDomRedraw();
		},
		change: function () {
			adjustSliders();
			refreshRetirementVariables();
			invalidRetirementValueCheck();
		}
	});
});