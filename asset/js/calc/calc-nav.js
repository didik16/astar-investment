$(function() {
	//get the url path
	var loc = window.location.pathname;

	//get rid of the slashes and put it into an array
	var divided = loc.split("/");

	//count the array items
	var itemCount = divided.length;

	//find the array position of the last item
	var arrayPosition = itemCount - 1;

	//find the array position of the second to last item 
	var arrayPositionParent = itemCount - 2;

	//this is what we will be looking for in the hrefs
	var activeId = divided[arrayPosition];

	//parent folder of the page - sets active nav if the page is index
	var parentFolder = divided[arrayPositionParent];

	//handler for index pages
	if (activeId == "" && parentFolder == "calculators") {
		activeId = "retirement-savings-calculator.html"
	}
	if (activeId == "index.html" && parentFolder == "calculators") {
		activeId = "retirement-savings-calculator.html"
	}

	if (activeId == "" && parentFolder == "calculatrices") {
		activeId = "calculatrice-epargne-retraite.html"
	}

	if (activeId == "index.html" && parentFolder == "calculatrices") {
		activeId = "calculatrice-epargne-retraite.html"
	}


	//find the href in the DOM and add the active tab class to the parent li 
	$('#calc-nav li a[href$="'+activeId+'"]').parent().addClass('active-calc');
});